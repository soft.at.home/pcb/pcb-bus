/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef PCB_SYSUTILS_H
#define PCB_SYSUTILS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <components.h>
#include <pcb/utils.h>
#include <pcb/core.h>

typedef struct _listen_socket {
    char* address;
    char* port;
    char* username;
    uint32_t defaultUid;
    bool trusted;
    peer_info_t* peer;
    llist_iterator_t it;
} listen_socket_t;

bool pcb_sysbus_setAppname(char* argv0);
char* pcb_sysbus_createPidFile();
#ifdef CONFIG_PCB_ACL_USERMNGT
uint32_t pcb_sysbus_getGroupId(const char* name);
#endif
void pcb_sysbus_setTCPKeepAlive(peer_info_t* peer);
listen_socket_t* pcb_sysbus_addListenSocket(const char* address, const char* port, bool trusted, const char* username, uint32_t userId);
peer_info_t* pcb_sysbus_createListenSocket(listen_socket_t* info);
bool pcb_sysbus_removeListenSocket(const char* address, const char* port);
bool pcb_sysbus_listen();
notification_t* pcb_sysbus_createApplicationStarted(object_t* root, object_t* proc);
notification_t* pcb_sysbus_createApplicationStopping(object_t* root, object_t* proc);
void pcb_sysbus_sniffer(request_t* req);

#ifdef __cplusplus
}
#endif

#endif // PCB_SYSUTILS_H
