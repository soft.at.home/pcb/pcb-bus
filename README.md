# pcb-bus

## Summary

This application acts as an PCB system bus.
It connects all PCB plug-ins connected to the bus
and forwards requests between them.

## Description

The PCB system bus is the application that all other
PCB plug-ins from the same PCB ecosystem connect to.
The system bus will allow communication between the
different plug-ins and forward requests and their replies
towards each other.

It also provides its own data model with information about
each connected plug-in, some debugging tools and the
possibility to connect busses to each other.
