-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/pcb-bus
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = pcb-bus

compile:
	$(MAKE) -C src all
	$(MAKE) -C scripts all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C scripts clean

install:
	$(INSTALL) -D -p -m 0755 src/pcb_sysbus $(D)/bin/pcb_sysbus
	$(INSTALL) -D -p -m 0755 scripts/sysbus.sh $(D)$(INITDIR)/sysbus
	$(INSTALL) -D -p -m 0755 scripts/pcb_common.sh $(D)/usr/bin/pcb_common.sh

.PHONY: compile clean install


