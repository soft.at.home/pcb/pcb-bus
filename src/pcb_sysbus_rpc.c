/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <components.h>
#include <usermngt/usermngt.h>

#include "pcb_sysdatamodel.h"
#include "parse_args.h"

#include <pcb/core/types.h>
#include <pcb/core/notification.h>
#include "pcb_sysdispatching.h"
#include "pcb_sysdebug_verify.h"
#include "pcb_sysutils.h"

extern application_config_t appconfig;

static peer_info_t* connection_findURI(connection_info_t* con, const char* URI) {
    uri_t* uri = NULL;
    peer_info_t* peer = NULL;

    if(!URI) {
        goto exit;
    }

    if(!(uri = uri_parse(URI))) {
        goto exit;
    }

    if(strcmp(uri_getHost(uri), "ipc") == 0) {
        peer = connection_findIPC(con, uri_getPort(uri));
    } else {
        peer = connection_findTCP(con, uri_getHost(uri), uri_getPort(uri));
    }

exit:
    uri_destroy(uri);
    return peer;
}

static bool sysbus_interconnectClosed(peer_info_t* peer) {
    SAH_TRACE_IN();

    peer_destroy(peer);

    SAH_TRACE_OUT();
    return true;
}

static function_exec_state_t sysbus_interconnect(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    pcb_t* pcb = datamodel_pcb(object_datamodel(fcall_object(fcall)));
    uint32_t attr = request_attributes(fcall_request(fcall));
    char* uri = NULL;
    bool trusted = false;
    bool synchronize = true;
    uri_t* parsed_uri = NULL;
    notification_t* notif = NULL;

    // take arguments
    argument_getChar(&uri, args, attr, "uri", NULL);
    argument_getBool(&trusted, args, attr, "trusted", false);
    argument_getBool(&synchronize, args, attr, "synchronize", true);

    if(connection_findURI(pcb_connection(pcb), uri)) {
        SAH_TRACE_ERROR("Bus already interconnected to %s", uri);
        goto exit_error;
    }

    peer_info_t* peer = connection_connect(pcb_connection(pcb), uri, &parsed_uri);
    if(!peer) {
        SAH_TRACE_ERROR("Failed to connect to %s", uri);
        goto exit_error;
    }

    // set the keep alive options and enable keep alive
    pcb_sysbus_setTCPKeepAlive(peer);

    peer_addCloseHandler(peer, sysbus_interconnectClosed);
    pcb_setTrusted(pcb, peer, trusted);
    pcb_setDefaultUid(pcb, peer, trusted ? 0 : UINT32_MAX);

    notif = notification_createBusInterconnect(connection_name(pcb_connection(pcb)), synchronize);
    pcb_sendNotification(pcb, peer, NULL, notif);
    notification_destroy(notif);

    free(uri);
    uri_destroy(parsed_uri);
    variant_setBool(retval, true);
    SAH_TRACE_OUT();
    return function_exec_done;

exit_error:
    free(uri);
    uri_destroy(parsed_uri);
    variant_setBool(retval, false);
    SAH_TRACE_OUT();
    return function_exec_error;
}

static function_exec_state_t sysbus_removeListen(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    uint32_t attr = request_attributes(fcall_request(fcall));
    char* uri = NULL;

    // take arguments
    argument_getChar(&uri, args, attr, "uri", NULL);

    uri_t* parsed_uri = uri_parse(uri);
    if(!parsed_uri) {
        SAH_TRACE_ERROR("Failed to parse uri %s", uri);
        goto exit_error;
    }

    if(pcb_sysbus_removeListenSocket(uri_getHost(parsed_uri), uri_getPort(parsed_uri)) == false) {
        SAH_TRACE_ERROR("Failed to remove socket");
        goto exit_error;
    }

    uri_destroy(parsed_uri);
    free(uri);
    variant_setBool(retval, true);
    SAH_TRACE_OUT();
    return function_exec_done;

exit_error:
    free(parsed_uri);
    free(uri);
    variant_setBool(retval, false);
    SAH_TRACE_OUT();
    return function_exec_error;
}

static function_exec_state_t sysbus_listen(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    uint32_t attr = request_attributes(fcall_request(fcall));
    char* uri = NULL;
    char* device = NULL;
    char* username = NULL;
    uint32_t userid = UINT32_MAX;
    bool trusted = false;
    peer_info_t* listen = NULL;
    listen_socket_t* info = NULL;
    const usermngt_user_t* usr = NULL;

    // take arguments
    argument_getChar(&uri, args, attr, "uri", NULL);
    argument_getBool(&trusted, args, attr, "trusted", false);
    argument_getChar(&device, args, attr, "interface", NULL);
    argument_getChar(&username, args, attr, "username", NULL);

    uri_t* parsed_uri = uri_parse(uri);
    if(!parsed_uri) {
        SAH_TRACE_ERROR("Failed to parse uri %s", uri);
        goto exit_error;
    }

    if(username) {
        usr = usermngt_userFindByName(username);
        if(!usr) {
            SAH_TRACE_ERROR("User %s does not exist", username);
            goto exit_error;
        }
        userid = usermngt_userID(usr);
    }

    info = pcb_sysbus_addListenSocket(uri_getHost(parsed_uri), uri_getPort(parsed_uri), trusted, username, trusted ? 0 : userid);
    if(!info) {
        SAH_TRACE_ERROR("Failed to add listen socket");
        goto exit_error;
    }
    listen = pcb_sysbus_createListenSocket(info);
    if(!listen) {
        SAH_TRACE_ERROR("Failed to open listen socket");
        goto exit_error;
    }

    if(device) {
        int fd = peer_getFd(listen);
        if(fd >= 0) {
            if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, device, strlen(device)) < 0) {
                SAH_TRACE_ERROR("Failed to bind to interface (%s)", device);
            }
        }
    }

    free(device);
    free(username);
    uri_destroy(parsed_uri);
    free(uri);
    variant_setBool(retval, true);
    SAH_TRACE_OUT();
    return function_exec_done;

exit_error:
    free(device);
    free(username);
    free(parsed_uri);
    free(uri);
    variant_setBool(retval, false);
    SAH_TRACE_OUT();
    return function_exec_error;
}

static function_exec_state_t sysbus_setAcl(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    uint32_t attr = request_attributes(fcall_request(fcall));
    char* object = NULL;
    char* name = NULL;
    uint32_t access = false;
    object_t* dm_object = NULL;
    uint32_t id = 0;

    // take arguments
    argument_getChar(&object, args, attr, "object", NULL);
    argument_getChar(&name, args, attr, "name", NULL);
    argument_getUInt32(&access, args, attr, "access", 0);

    if(!(*object) || !(*name)) {
        // empty object or empty name not allowed, exit
        goto exit_error;
    }

    dm_object = object_getObjectByKey(object_root(fcall_object(fcall)), "%s", object);

    if(access & acl_group_id) {
        const usermngt_group_t* gr = usermngt_groupFindByName(name);
        if(gr == NULL) {
            SAH_TRACE_WARNING("unable to fetch id for group %s", name);
            goto exit_error;
        }
        SAH_TRACE_INFO("Group %s: id = %d", name, usermngt_groupID(gr));
        id = usermngt_groupID(gr);
    } else {
        const usermngt_user_t* pw = usermngt_userFindByName(name);
        if(pw == NULL) {
            SAH_TRACE_WARNING("unable to fetch id for user %s", name);
            goto exit_error;
        }
        SAH_TRACE_INFO("User %s: id = %d", name, usermngt_userID(pw));
        id = usermngt_userID(pw);
    }

    object_aclSet(dm_object, id, access);

    free(object);
    free(name);
    variant_setBool(retval, true);
    SAH_TRACE_OUT();
    return function_exec_done;

exit_error:
    free(object);
    free(name);
    variant_setBool(retval, false);
    SAH_TRACE_OUT();
    return function_exec_error;
}

bool pcb_sysbus_createBusDefinition(pcb_t* pcb) {
    object_t* root = datamodel_root(pcb_datamodel(pcb));

    // create Debug object
    object_t* bus = object_create(root, "Bus", 0);
    if(!bus) {
        object_rollback(root);
        return false;
    }
    object_aclSet(bus, UINT32_MAX, 0);

    // add function interconnect
    function_t* fn = function_create(bus, "interconnect", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_interconnect);
    argument_create(fn, "uri", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "trusted", argument_type_bool, 0);
    argument_create(fn, "synchronize", argument_type_bool, 0);

    // add function listen
    fn = function_create(bus, "listen", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_listen);
    argument_create(fn, "uri", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "trusted", argument_type_bool, 0);
    argument_create(fn, "interface", argument_type_string, 0);
    argument_create(fn, "username", argument_type_string, 0);

    // add function stopListen
    fn = function_create(bus, "removeListen", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_removeListen);
    argument_create(fn, "uri", argument_type_string, argument_attr_mandatory);

    // add function disconnect
    fn = function_create(bus, "disconnect", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, pcb_sysbus_processUnregister);
    argument_create(fn, "process", argument_type_string, argument_attr_mandatory);

    // add function setACL
    fn = function_create(bus, "setAcl", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_setAcl);
    argument_create(fn, "object", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "name", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "access", argument_type_uint32, argument_attr_mandatory);

    return true;
}

