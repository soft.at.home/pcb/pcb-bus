/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pcb/core/object.h>

#include "pcb_sysutils.h"
#include "pcb_sysdatamodel.h"
#include "pcb_sysdispatching.h"
#include "pcb_sysrequesthandlers.h"

bool pcb_sysbus_dispatchObjectCreate(pcb_t* pcb, peer_info_t* peer, const char* name, uint32_t attributes, system_object_type_t type, object_t* process) {
    if(!name || !type) {
        return false;
    }
    parameter_t* param = NULL;
    system_object_info_t* info = NULL;
    system_object_info_t* process_info = NULL;
    object_t* root = datamodel_root(pcb_datamodel(pcb));

    SAH_TRACE_INFO("Adding dispatch object [%s]", name);

    object_t* dispatcher = object_getObjectByKey(root, name);
    if(dispatcher) {
        SAH_TRACE_NOTICE("   => Object [%s] is duplicated (object = %s)", name, object_name(dispatcher, path_attr_key_notation));
        info = object_getUserData(dispatcher);
        // check if the existing one is from a bus interconnect or not
        if(!info ||
           (info->type == system_object_internal) ||
           (info->type == system_object_self)) {
            SAH_TRACE_NOTICE("   => Object [%s] is an internal object", name);
            SAH_TRACE_NOTICE("   => acton = skip");
            goto exit_skip;
        }

        process_info = object_getUserData(process);
        if(process_info &&
           process_info->process &&
           (strcmp(object_da_parameterCharValue(process_info->process, "Type"), "Bus") == 0)) {
            // new object is from a bus interconnect, skip it
            SAH_TRACE_NOTICE("   => New object [%s] is inter-connected (%s->%s)",
                             name,
                             object_name(process_info->process, path_attr_key_notation),
                             object_name(process, path_attr_key_notation));
            SAH_TRACE_NOTICE("   => action = skip, keep current dispatch info (%s)",
                             object_name(info->process, path_attr_key_notation));
            goto exit_skip;
        }

        process_info = object_getUserData(info->process);
        if(process_info &&
           process_info->process &&
           (strcmp(object_da_parameterCharValue(process_info->process, "Type"), "Bus") == 0)) {
            // current object is from a bus interconnect, overwrite
            SAH_TRACE_NOTICE("   => New object [%s] is local",
                             name);
            SAH_TRACE_NOTICE("   => action = overwrite, current is inter-connected (%s->%s)",
                             object_name(process_info->process, path_attr_key_notation),
                             object_name(info->process, path_attr_key_notation));
            goto leave;
        }

        // if not a bus interconnect set error and leave
        SAH_TRACE_ERROR("Duplicate object names %s, do not add", name);
        info = NULL;
        pcb_error = pcb_error_not_unique_name;
        goto exit_error;
    }

    // create system information
    info = (system_object_info_t*) calloc(1, sizeof(system_object_info_t));
    if(!info) {
        SAH_TRACE_ERROR("Failed to create system information");
        pcb_error = pcb_error_no_memory;
        goto exit_error;
    }

    dispatcher = object_create(root, name, attributes & ~object_attr_remote);
    if(!dispatcher) {
        SAH_TRACE_ERROR("Failed to create dispatching object");
        goto exit_error;
    }

    object_setUserData(dispatcher, info);
    object_setDestroyHandler(dispatcher, pcb_sysbus_objectDestroy);

leave:
    info->type = type;
    info->peer = peer;
    info->process = process;

    return true;

exit_skip:
    return true;

exit_error:
    param = object_getParameter(process, "Status");
    if(param) {
        char* errorstr = NULL;
        if(asprintf(&errorstr, "Error: (0x%8.8X) %s", pcb_error, error_string(pcb_error)) != -1) {
            parameter_setFromChar(param, errorstr);
        } else {
            parameter_setFromChar(param, "Error: Creating dispatch object");
        }
        free(errorstr);
        object_commit(process);
    }
    free(info);
    return false;
}

static bool pcb_sysbus_replyHandler(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    object_t* process = (object_t*) userdata;

    reply_t* reply = request_reply(req);
    reply_item_t* item = reply_firstItem(reply);
    if(!reply) {
        goto exit_reply;
    }

    while(item) {
        switch(reply_item_type(item)) {
        case reply_type_object: {
            object_t* object = reply_item_object(item);
            const char* name = object_name(object, 0);
            if(name && *name) {
                SAH_TRACE_INFO("Adding object %s", name);
                if(!pcb_sysbus_dispatchObjectCreate(pcb, from, name, object_attributes(object), system_object_dispatch, process)) {
                    SAH_TRACE_ERROR("Failed to add dispatch object, continue with others ...");
                }
                if(strcmp(object_da_parameterCharValue(process, "Type"), "Bus") != 0) {
                    object_parameterSetCharValue(process, "Type", "Plug-in");
                    object_commit(process);
                }
            } else {
                SAH_TRACE_INFO("Skipping root from plug-in");
            }
        }
        break;
        case reply_type_notification: {
            SAH_TRACE_WARNING("Unexpected notification");
        }
        break;
        case reply_type_error: {
            SAH_TRACE_ERROR("Error received : 0x%8.8X %s %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            parameter_t* param = object_getParameter(process, "Status");
            if(param) {
                char* errorstr = NULL;
                if(asprintf(&errorstr, "Error: (0x%8.8X) %s - %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item)) != -1) {
                    parameter_setFromChar(param, errorstr);
                } else {
                    parameter_setFromChar(param, "Error: Error received from plug-in");
                }
                free(errorstr);
                object_commit(process);
            }
        }
        break;
        default:
            break;
        }
        item = reply_nextItem(item);
    }

exit_reply:
    return true;
}

void pcb_sysbus_forward_notifyRequests(object_t* root, peer_info_t* to) {
    request_t* notify_request = object_firstNotifyRequest(root);
    request_t* prefetch = object_nextNotifyRequest(root, notify_request);
    while(notify_request) {
        uint32_t depth = request_depth(notify_request);
        uint32_t attributes = request_attributes(notify_request);
        const char* path = request_path(notify_request);
        request_source_info_t* req_info = request_data(notify_request);
        if(path && *path) {
            SAH_TRACE_INFO("Request with path was stored, need forwarding ????");
            bool exactMatch = false;
            bool local_only = false;
            int offset = 0;

            const char* dm_namespace = datamodel_namespace(object_datamodel(root));
            SAH_TRACE_INFO("bus name = %s, path = %s", dm_namespace, path);
            if(strncmp(path, dm_namespace, strlen(dm_namespace)) == 0) {
                SAH_TRACE_INFO("Only search for local object");
                local_only = true;
                offset = strlen(dm_namespace) + 1;
            }

            uint32_t localAttributes = attributes & (path_attr_key_notation | path_attr_slash_seperator);
            localAttributes |= path_attr_partial_match;
            SAH_TRACE_INFO("Search object %s", path + offset);
            object_t* object = object_getObject(root, path + offset, localAttributes, &exactMatch);
            if(!object) {
                SAH_TRACE_INFO("  ==> NO : Not found");
                notify_request = prefetch;
                prefetch = object_nextNotifyRequest(root, notify_request);
                continue;
            }
            system_object_info_t* info = object_getUserData(object);
            if(info) {
                SAH_TRACE_INFO("process info - type = %d", info->type);
                SAH_TRACE_INFO("process info - process = %s", object_name(info->process, path_attr_key_notation));
                system_object_info_t* proc_info = object_getUserData(info->process);
                SAH_TRACE_INFO("parent process = %p", proc_info);
                if(proc_info && proc_info->process && local_only) {
                    SAH_TRACE_NOTICE("  == NO : Object from another bus, should be local");
                    notify_request = prefetch;
                    prefetch = object_nextNotifyRequest(root, notify_request);
                    continue;
                }
                if(info->peer != to) {
                    SAH_TRACE_NOTICE("Dispatch info socket (%p) and to socket (%p) not matching -> skip", info->peer, to);
                    notify_request = prefetch;
                    prefetch = object_nextNotifyRequest(root, notify_request);
                    continue;
                }
            } else {
                SAH_TRACE_NOTICE("Object (%s) does not contain dispatch info (skip)", object_name(object, path_attr_key_notation));
                notify_request = prefetch;
                prefetch = object_nextNotifyRequest(root, notify_request);
                continue;
            }
            SAH_TRACE_INFO("  ==> YES");
            object_addNotifyRequest(object, notify_request);
            request_t* copy_req = request_copy(notify_request, depth, attributes, request_copy_default);
            if(local_only) {
                request_setPath(copy_req, request_path(notify_request) + strlen(dm_namespace));
            }
            if(req_info) {
                SAH_TRACE_INFO("Forwarding notify request");
                pcb_sysbus_forwardRequest(notify_request, req_info->source_peer, copy_req, to);
            }
        } else {
            object_t* interconnected = pcb_sysbus_getInterconnection(to, object_getObjectByKey(root, "Process"));
            if(!interconnected) {
                request_t* copy_req = request_copy(notify_request, depth, attributes, request_copy_default);
                if(req_info) {
                    SAH_TRACE_INFO("Forwarding notify request");
                    pcb_sysbus_forwardRequest(notify_request, req_info->source_peer, copy_req, to);
                }
            } else {
                SAH_TRACE_NOTICE("Notify request with empty path, do not forward to interconnected bus");
            }
        }
        notify_request = prefetch;
        prefetch = object_nextNotifyRequest(root, notify_request);
    }
}

void pcb_sysbus_dispatchObjectDelete(pcb_t* pcb, peer_info_t* peer, object_t* proc) {
    object_t* root = datamodel_root(pcb_datamodel(pcb));

    object_t* object = object_firstChild(root);
    object_t* prefetch = object_nextSibling(object);
    while(object) {
        system_object_info_t* info = (system_object_info_t*) object_getUserData(object);
        if(info && ((info->type == system_object_dispatch) || (info->type == system_object_dispatch_namespace))) {
            if(proc) {
                if(proc == info->process) {
                    info->peer = NULL;
                    info->process = NULL;
                    SAH_TRACE_INFO("delete dispatch object");
                    object_delete(object);
                }
            } else if(info->peer == peer) {
                info->peer = NULL;
                info->process = NULL;
                SAH_TRACE_INFO("delete dispatch object");
                object_delete(object);
            }
        }
        object = prefetch;
        prefetch = object_nextSibling(object);
    }

    object_commit(root);
}

bool pcb_sysbus_requestDone(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) pcb;
    if(userdata) {
        object_t* process = (object_t*) userdata;
        notification_t* notification = pcb_sysbus_createApplicationStarted(datamodel_root(pcb_datamodel(pcb)), process);
        pcb_sendNotification(pcb, NULL, from, notification);
        notification_destroy(notification);

        const char* currentStatus = object_da_parameterCharValue(process, "Status");
        if(currentStatus && (strncmp(currentStatus, "Error:", 6) != 0)) {
            parameter_t* param = object_getParameter(process, "Status");
            if(param) {
                parameter_setFromChar(param, "Ready");
            }
        }
    }
    // destroy the request
    request_destroy(req);

    object_t* root = datamodel_root(pcb_datamodel(pcb));
    // this must be done before commiting
    pcb_sysbus_forward_notifyRequests(root, from);

    object_commit(root);

    return true;
}

bool pcb_sysbus_loadDispatchingObjects(pcb_t* pcb, peer_info_t* peer, object_t* process, notification_t* notification) {
    bool object = false;
    const char* type = "Client";
    notification_parameter_t* nparam = notification_firstParameter(notification);
    while(nparam) {
        if(strcmp(notification_parameter_name(nparam), "Object") == 0) {
            char* name = notification_parameter_value(nparam);
            if(name && *name) {
                if(!pcb_sysbus_dispatchObjectCreate(pcb, peer, name, object_attr_default, system_object_dispatch, process)) {
                    SAH_TRACE_ERROR("Failed to add dispatch object");
                } else {
                    type = "Plug-in";
                }
            }
            object = true;
            free(name);
        }
        nparam = notification_nextParameter(nparam);
    }
    if(!object) {
        object_t* processes = pcb_getObject(pcb, "Process", path_attr_default);
        object_t* interconnect = pcb_sysbus_getInterconnection(peer, processes);
        if(!interconnect) {
            request_t* get = request_create_getObject("", 0, request_no_object_caching | request_getObject_children);
            request_setData(get, process);
            request_setReplyHandler(get, pcb_sysbus_replyHandler);
            request_setDoneHandler(get, pcb_sysbus_requestDone);

            if(!pcb_sendRequest(pcb, peer, get)) {
                SAH_TRACE_NOTICE("Failed to send request");
            }

            object_parameterSetCharValue(process, "Status", "Fetching objects");
            object_parameterSetCharValue(process, "Type", type);
        } else {
            object_parameterSetCharValue(process, "Status", "Ready");
            object_parameterSetCharValue(process, "Type", type);
        }
        SAH_TRACE_INFO("Process %s added, type = %s", object_name(process, path_attr_key_notation), type);
        object_commit(process);
    } else {
        object_t* root = datamodel_root(pcb_datamodel(pcb));
        if(notification_type(notification) == notify_application_started) {
            if(strncmp(object_da_parameterCharValue(process, "Status"), "Error:", 6) != 0) {
                object_parameterSetCharValue(process, "Status", "Ready");
                // forward notification
                SAH_TRACE_INFO("Forwarding notification");
                pcb_sendNotification(pcb, NULL, peer, notification);
            }
            object_parameterSetCharValue(process, "Type", type);
        } else {
            object_parameterSetCharValue(process, "Status", "Ready");
        }

        // this must be done before commiting
        SAH_TRACE_INFO("Forwarding pending (notification) requests");
        pcb_sysbus_forward_notifyRequests(root, peer);
        object_commit(root);
    }

    return true;
}

bool pcb_sysbus_addDispatchObject(pcb_t* pcb, const char* name, peer_info_t* peer, object_t* process, notification_t* notification, const char* type) {
    (void) notification;
    SAH_TRACE_INFO("Adding object %s", name);
    object_t* root = NULL;
    if(!pcb_sysbus_dispatchObjectCreate(pcb, peer, name, object_attr_default, system_object_dispatch_namespace, process)) {
        goto exit_error;
    }

    object_parameterSetCharValue(process, "Status", "Ready");
    object_parameterSetCharValue(process, "Type", type);

    // forward notification
    //pcb_sendNotification(pcb,NULL,peer,notification);
    // update outstanding notification requests
    root = datamodel_root(pcb_datamodel(pcb));
    // this must be done before commiting
    pcb_sysbus_forward_notifyRequests(root, peer);

    object_commit(datamodel_root(pcb_datamodel(pcb)));

    return true;
exit_error:
    return false;
}
