/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <unistd.h>
#include <stdio.h>
#define _GNU_SOURCE
#include <getopt.h>

#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>

#include "parse_args.h"

static struct option long_options[] = {
    {"help", 0, 0, 'h'},
    {"verbose", 1, 0, 'v'},
    {"ipc", 1, 0, 'I'},
    {"tcp_address", 1, 0, 'A'},
    {"tcp_port", 1, 0, 'P'},
    {"name", 1, 0, 'n'},
    {"output", 1, 0, 'o'},
    {"forground", 1, 0, 'f'},
    {"priority", 1, 0, 'p'},
    {"sniff", 1, 0, 's'},
    {0, 0, 0, 0}
};

application_config_t appconfig = {
    .trace_level = 0,
    .trace_output = TRACE_TYPE_SYSLOG,
    .ipc_socket_name = NULL,
    .tcp_address = NULL,
    .tcp_port = NULL,
    .name = "pcb_sysbus",
    .pcb = NULL,
    .foreground = false,
    .config_flags = config_default,
    .priority = 0,
    .sniff = false
};

void print_help(const char* appname) {
    printf("\n");
    printf("Usage: %s [OPTIONS] \n", appname);
    printf("Start a PCB system bus\n");
    printf("\n");
    printf("Mandatory arguments to long options are mandatory for short options too.\n");
    printf("\nOPTIONS\n");
    printf("   -h, --help                     Print this help\n");
    printf("   -v, --verbose[=level]          Increase the debug output level\n");
    printf("   -f, --foreground               Keep in foreground\n");
    printf("   -o, --output=stdout|syslog     Trace output destination\n");
    printf("   -I  --ipc=name                 IPC listen socket name\n");
    printf("   -A  --tcp_address=address      TCP listen socket address\n");
    printf("   -P  --tcp_port=port            TCP listen socket port\n");
    printf("   -n  --name=name                Connection name, used in debugging information\n");
    printf("   -p  --priority=PRIOR           Set the nice value\n");
    printf("   -s  --sniff                    Global sniffing is on\n");
    printf("\n");
}

int parse_arguments(int argc, char* const argv[]) {
    int c;
    int option_index = 0;

    while(true) {
        c = getopt_long(argc, argv, "hvfso:I:A:P:n:p:", long_options, &option_index);

        if(c == -1) {
            break;
        }

        SAH_TRACE_INFO("Parsing option %X (%c) (value = %s, option index = %d)", c, c, optarg, option_index);
        switch(c) {
        case 'n':
            appconfig.name = optarg;
            break;
        case 'v':
            if(optarg) {
                appconfig.trace_level += (uint32_t) atol(optarg);
            } else {
                appconfig.trace_level += 100;
            }
            break;
        case 'f':
            appconfig.foreground = true;
            appconfig.trace_output = TRACE_TYPE_STDOUT;
            break;
        case 'I':
            appconfig.ipc_socket_name = optarg;
            break;
        case 'A':
            appconfig.tcp_address = optarg;
            break;
        case 'P':
            appconfig.tcp_port = optarg;
            break;
        case 'o':
            if(optarg) {
                if(strcmp(optarg, "syslog") == 0) {
                    appconfig.trace_output = TRACE_TYPE_SYSLOG;
                } else if(strcmp(optarg, "stdout") == 0) {
                    appconfig.trace_output = TRACE_TYPE_STDOUT;
                }
            } else {
                appconfig.trace_output = TRACE_TYPE_STDOUT;
            }
            break;
        case 'p':
            appconfig.priority = atoi(optarg);
            break;
        case 's':
            appconfig.sniff = true;
            break;
        case '?':
        default:
        case 'h':
            print_help(argv[0]);
            return 0;
            break;
        }
    }

    return optind;
}
