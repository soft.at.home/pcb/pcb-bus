/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <components.h>

#include "pcb_sysdatamodel.h"
#include "parse_args.h"

#include <pcb/core/types.h>
#include <pcb/core/notification.h>
#include "pcb_sysdispatching.h"
#include "pcb_sysdebug_verify.h"

extern application_config_t appconfig;

static void sysbus_verifyEnd(verify_data_t* data) {
    function_call_t* fcall = data->fcall;

    peer_info_t* peer = fcall_peer(fcall);
    request_t* req = fcall_request(fcall);
    // remove cancel handler
    fcall_setCancelHandler(fcall, NULL);

    // generate reply
    pcb_replyBegin(peer, req, 0);
    pcb_writeFunctionReturnBegin(peer, req);
    pcb_writeReturnValue(peer, req, &data->retval);
    pcb_writeFunctionReturnEnd(peer, req);
    pcb_replyEnd(peer, req);

    // cleanup
    variant_cleanup(&data->retval);
    pcb_timer_destroy(data->timer);
    free(data->current_obj_name);
    free(data);

    request_setDone(req);
    fcall_destroy(fcall);
}

static bool sysbus_verifyReplyHandler(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) req;
    (void) pcb;
    (void) from;

    if(!userdata) {
        return false;
    }

    verify_data_t* data = (verify_data_t*) userdata;
    pcb_timer_start(data->timer, data->timeout);
    return true;
}

static void sysbus_verifyAddStatus(variant_t* retval, object_t* proc, const char* status) {
    variant_map_t* map = variant_da_map(retval);
    const char* proc_name = object_da_parameterCharValue(proc, "Name");
    variant_map_addChar(map, proc_name, status);
}

static bool sysbus_verifyRequestDone(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) req;
    (void) pcb;
    (void) from;

    if(!userdata) {
        return false;
    }

    verify_data_t* data = (verify_data_t*) userdata;
    // request timed out
    request_destroy(data->request);

    object_t* current = object_getObjectByKey(datamodel_root(pcb_datamodel(appconfig.pcb)), "Process.%s", data->current_obj_name);
    if(!current) {
        sysbus_verifyEnd(data);
        return true;
    }

    // add result to map
    sysbus_verifyAddStatus(&data->retval, current, "OK");

    object_t* proc = object_nextInstance(current);
    if(!proc) {
        sysbus_verifyEnd(data);
        return true;
    }
    if(!sysbus_verify(proc, data)) {
        sysbus_verifyEnd(data);
    }
    return true;
}

static object_t* sysbus_next_verify_object(object_t* proc) {
    system_object_info_t* info = object_getUserData(proc);
    SAH_TRACE_INFO("Verify process %s", object_name(proc, path_attr_key_notation));
    while(proc) {
        info = object_getUserData(proc);
        if(!info) {
            SAH_TRACE_ERROR("Process %s doesn't contain any info", object_name(proc, path_attr_key_notation));
            proc = object_nextInstance(proc);
            continue;
        }
        SAH_TRACE_INFO("%s needs verification?", object_name(proc, path_attr_key_notation));
        // skip processes connected using TCP socket
        if(peer_isTCPSocket(info->peer)) {
            SAH_TRACE_INFO("=> NO - connected using TCP");
            proc = object_nextInstance(proc);
            continue;
        }
        // skip processes with parent process
        if(info->process) {
            SAH_TRACE_INFO("=> NO - has parent process");
            proc = object_nextInstance(proc);
            continue;
        }
        SAH_TRACE_INFO("=> YES");
        break;
    }
    return proc;
}

bool sysbus_verify(object_t* proc, verify_data_t* data) {
    proc = sysbus_next_verify_object(proc);
    if(!proc) {
        return false;
    }
    system_object_info_t* info = object_getUserData(proc);
    char* status = object_parameterCharValue(proc, "Status");
    while(!info || !info->peer || (status && strcmp(status, "Ready") != 0)) {
        SAH_TRACE_INFO("info = %p, info->peer = %p, status = %s", info, info ? info->peer : NULL, status);
        if(strcmp(status, "Ready") != 0) {
            sysbus_verifyAddStatus(&data->retval, proc, status);
        } else {
            if(info) {
                if(info->type == system_object_self) {
                    sysbus_verifyAddStatus(&data->retval, proc, "OK");
                } else {
                    sysbus_verifyAddStatus(&data->retval, proc, "Broken pipe");
                }
            } else {
                sysbus_verifyAddStatus(&data->retval, proc, "Missing info");
            }
        }
        free(status);
        proc = object_nextInstance(proc);
        proc = sysbus_next_verify_object(proc);
        if(!proc) {
            return false;
        }
        info = object_getUserData(proc);
        status = object_parameterCharValue(proc, "Status");
    }
    free(status);

    free(data->current_obj_name);
    data->current_obj_name = strdup(object_name(proc, path_attr_key_notation));

    // create verification request
    data->request = request_create_getObject("", data->depth, request_no_object_caching);
    request_setData(data->request, data);
    request_setReplyHandler(data->request, sysbus_verifyReplyHandler); // the reply handler will do nothing, except resetting the timer
    request_setDoneHandler(data->request, sysbus_verifyRequestDone);   // stop the timer, take next process and send request

    // send the request
    if(!pcb_sendRequest(appconfig.pcb, info->peer, data->request)) {
        SAH_TRACE_NOTICE("Failed to send request");
    }
    // start the timer
    pcb_timer_start(data->timer, data->timeout);

    return true;
}

void sysbus_verifyTimeout(pcb_timer_t* timer, void* userdata) {
    (void) timer;
    if(!userdata) {
        return;
    }
    verify_data_t* data = (verify_data_t*) userdata;
    // request timed out
    request_destroy(data->request);

    object_t* current = object_getObjectByKey(datamodel_root(pcb_datamodel(appconfig.pcb)), "Process.%s", data->current_obj_name);
    if(!current) {
        sysbus_verifyEnd(data);
        return;
    }

    // add result to map
    sysbus_verifyAddStatus(&data->retval, current, "Not responding");

    object_t* proc = object_nextInstance(current);
    if(!proc) {
        sysbus_verifyEnd(data);
        return;
    }
    if(!sysbus_verify(proc, data)) {
        sysbus_verifyEnd(data);
    }
}

void sysbus_verifyCanceled(function_call_t* fcall, void* userdata) {
    verify_data_t* data = (verify_data_t*) userdata;
    // remove all user data
    pcb_timer_setUserData(data->timer, NULL);
    request_setData(data->request, NULL);
    // stop timer, destroy request
    pcb_timer_stop(data->timer);
    request_destroy(data->request);

    // cleanup
    variant_cleanup(&data->retval);
    pcb_timer_destroy(data->timer);
    free(data);

    // delete the function call context
    fcall_setCancelHandler(fcall, NULL);
}
