/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <components.h>

#include "pcb_sysdatamodel.h"
#include "parse_args.h"

#include <pcb/core/types.h>
#include <pcb/core/notification.h>
#include "pcb_sysdispatching.h"
#include "pcb_sysutils.h"
#include "pcb_sysbus_rpc.h"
#include "pcb_sysbus_mtk.h"

extern application_config_t appconfig;
extern object_t* self;

static void pcb_sysbus_updateDatamodel(pcb_t* pcb, peer_info_t* from, notification_t* notification) {
    notification_parameter_t* param_ns = notification_getParameter(notification, "Namespace");
    char* value = notification_parameter_value(param_ns);
    object_t* process = NULL;
    process = pcb_sysbus_addProcess(pcb, from, notification);
    if(!process) {
        SAH_TRACE_ERROR("Process is not created, do not add dispatch objects");
        free(value);
        return;
    }
    // if notification contains a namespace parameter which is not empty
    // create a dispatch object with that name
    // otherwise load all root objects
    if(param_ns && value && *value) {
        SAH_TRACE_INFO("Add Namespace dispatch object = %s (%p)", value, param_ns);
        pcb_sysbus_addDispatchObject(pcb, value, from, process, notification, "Plug-in");
    } else {
        pcb_sysbus_loadDispatchingObjects(pcb, from, process, notification);
    }
    free(value);
}

static void pcb_sysbus_updateDispatch(pcb_t* pcb, peer_info_t* from, notification_t* notification, bool add) {
    object_t* instance = NULL;
    object_t* processes = pcb_getObject(pcb, "Process", path_attr_default);
    notification_parameter_t* nparam = NULL;
    object_t* root = datamodel_root(pcb_datamodel(pcb));

    // search matching process
    object_for_each_instance(instance, processes) {
        system_object_info_t* info = (system_object_info_t*) object_getUserData(instance);
        if(!info || !info->peer) {
            continue;
        }

        SAH_TRACE_INFO("Verify %p == %p", info->peer, from);
        if(info->peer == from) {
            break;
        }
    }

    // nothing found, leave
    if(!instance) {
        SAH_TRACE_WARNING("No process found for peer, dispatch object(s) not added");
        goto exit;
    }

    // all object names must be added
    nparam = notification_firstParameter(notification);
    while(nparam) {
        char* name = NULL;
        SAH_TRACE_INFO("Notification parameter = %s", notification_parameter_name(nparam));
        if(strcmp(notification_parameter_name(nparam), "Object") == 0) {
            name = notification_parameter_value(nparam);
            if(!name || !(*name)) {
                free(name);
                nparam = notification_nextParameter(nparam);
                continue;
            }
            if(add) {
                // add dispatch object
                if(!pcb_sysbus_dispatchObjectCreate(pcb, from, name, object_attr_default, system_object_dispatch, instance)) {
                    SAH_TRACE_ERROR("Failed to update dispatch table for object %s from process %s", name, object_name(instance, path_attr_key_notation));
                }
            } else {
                object_t* object = pcb_getObject(pcb, name, path_attr_key_notation);
                if(!object) {
                    SAH_TRACE_NOTICE("Object %s not found, ignore", name);
                } else {
                    // remove dispatch object
                    object_delete(object);
                }
            }
        }
        free(name);
        nparam = notification_nextParameter(nparam);
    }

    pcb_sendNotification(pcb, NULL, from, notification);
    // this must be done before commiting
    pcb_sysbus_forward_notifyRequests(root, from);
    // commit the root
    object_commit(root);

exit:
    return;
}

void pcb_sysbus_synchronise(pcb_t* pcb, peer_info_t* to) {
    SAH_TRACE_IN();
    object_t* root = datamodel_root(pcb_datamodel(pcb));
    object_t* processes = object_getObject(root, "Process", 0, NULL);
    object_t* proc = NULL;

    system_object_info_t* proc_info = NULL;
    notification_t* notify = NULL;
    object_for_each_instance(proc, processes) {
        proc_info = object_getUserData(proc);
        if(!proc_info ||
           (proc_info->type == system_object_self)) {
            SAH_TRACE_INFO("Do not synchronise self");
            continue;
        }
        if(strcmp(object_da_parameterCharValue(proc, "Type"), "Bus") == 0) {
            SAH_TRACE_INFO("Do not synchronise remote bus objects using the bus process");
            continue;
        }

        SAH_TRACE_INFO("Synchronise process %s", object_name(proc, path_attr_key_notation));
        // create application started notification, to synchronise with remote bus
        notify = pcb_sysbus_createApplicationStarted(root, proc);
        pcb_sendNotification(pcb, to, NULL, notify);
        peer_flush(to);
        notification_destroy(notify);
    }
    SAH_TRACE_OUT();
}

void pcb_sysbus_objectDestroy(object_t* object) {
    system_object_info_t* info = (system_object_info_t*) object_getUserData(object);
    if(info) {
        object_setUserData(object, NULL);
        free(info);
    }
}

bool pcb_sysbus_notifyHandler(pcb_t* pcb, peer_info_t* from, notification_t* notification) {
    SAH_TRACE_INFO("Dispatch notifications");

    switch(notification_type(notification)) {
    case notify_bus_interconnect: {
        object_t* process = NULL;
        SAH_TRACE_INFO("=> bus interconnect notification");
        process = pcb_sysbus_addProcess(pcb, from, notification);
        if(!process) {
            SAH_TRACE_ERROR("Process is not created, do not add dispatch objects");
            // close and cleanup the peer
            peer_destroy(from);
            return false;     // stop any futher processing
        }
        // add a name space object for the interconnected bus:
        notification_parameter_t* param_bus_name = notification_getParameter(notification, "BusName");
        char* proc_name = notification_parameter_value(param_bus_name);
        bool sync = true;
        notification_parameter_t* synchonize_param = notification_getParameter(notification, "Synchronize");
        if(synchonize_param) {
            sync = variant_bool(notification_parameter_variant(synchonize_param));
        }
        SAH_TRACE_INFO("=> Create namespace object %s", proc_name);
        pcb_sysbus_addDispatchObject(pcb, proc_name, from, process, notification, "Bus");
        free(proc_name);
        if(peer_isClientConnection(from)) {
            // send notify_bus_interconnect back containing info of this bus
            SAH_TRACE_INFO("=> send bus interconnect notification back");
            notification_t* notif = notification_createBusInterconnect(connection_name(pcb_connection(pcb)), sync);
            pcb_sendNotification(pcb, from, NULL, notif);
            notification_destroy(notif);
        }
        if(sync) {
            // iterate over all processes and create application started notifications (except for self)
            SAH_TRACE_INFO("=> synchronise content");
            pcb_sysbus_synchronise(pcb, from);
        } else {
            pcb_broadcastEnable(from, false);
        }
        object_parameterSetCharValue(process, "Status", "Ready");
        object_commit(process);
    }
    break;
    case notify_application_started:
        SAH_TRACE_INFO("=> Application started notification");
        pcb_sysbus_updateDatamodel(pcb, from, notification);
        break;
    case notify_application_stopping:
        SAH_TRACE_NOTICE("Application stopping recieved");
        SAH_TRACE_NOTICE("====> Delete/update processes");
        pcb_sysbus_deleteProcess(pcb, from, notification);
        pcb_sendNotification(pcb, NULL, from, notification);
        break;
    case notify_application_root_added:
        SAH_TRACE_NOTICE("Application root object added recieved");
        SAH_TRACE_NOTICE("====> Update dispatch table");
        pcb_sysbus_updateDispatch(pcb, from, notification, true);
        break;
    case notify_application_root_deleted:
        SAH_TRACE_NOTICE("Application root object deleted recieved");
        SAH_TRACE_NOTICE("====> Update dispatch table");
        pcb_sysbus_updateDispatch(pcb, from, notification, false);
        break;
    case notify_application_change_setting:
        SAH_TRACE_NOTICE("Application change setting recieved");
        SAH_TRACE_NOTICE("====> drop - no forwarding");
        break;
    default:
        pcb_sendNotification(pcb, NULL, from, notification);
        break;
    }

    return true;
}

bool pcb_sysbus_createDatamodel(pcb_t* pcb) {
    if(!pcb) {
        return false;
    }

    object_t* root = datamodel_root(pcb_datamodel(pcb));
    object_aclSet(root, UINT32_MAX, 0);

    if(!pcb_sysbus_createProcessesDefinition(pcb)) {
        return false;
    }

    if(!mtk_process_createDefinition(pcb)) {
        return false;
    }

    if(!pcb_sysbus_createDebugDefinition(pcb)) {
        return false;
    }

    if(!pcb_sysbus_createBusDefinition(pcb)) {
        return false;
    }

    object_commit(root);

    return true;
}

bool pcb_sysbus_addself() {
    object_t* processes = pcb_getObject(appconfig.pcb, "Process", path_attr_default);

    object_t* instance = object_getObjectByKey(processes, "%s", appconfig.name);
    if(!instance) {
        instance = object_createInstance(processes, UINT32_MAX, appconfig.name);
    }
    object_t* traces = object_getObject(instance, "Tracing", path_attr_default, NULL);

    object_parameterSetCharValue(instance, "Name", appconfig.name);
    object_parameterSetCharValue(instance, "Status", "Ready");
    object_parameterSetCharValue(instance, "Type", "Self");
    object_parameterSetInt32Value(instance, "PID", getpid());
    object_parameterSetCharValue(instance, "Connection", "Self");
    object_parameterSetInt32Value(instance, "Registered", true);
    object_parameterSetInt32Value(traces, "TraceLevel", sahTraceLevel());
    object_parameterSetInt32Value(traces, "Enabled", sahTraceIsOpen());

    sah_trace_zone_it* zone = sahTraceFirstZone();
    parameter_t* param = NULL;
    while(zone) {
        param = parameter_create(traces, sahTraceZoneName(zone), parameter_type_uint32, parameter_attr_persistent);
        object_parameterSetInt32Value(instance, sahTraceZoneName(zone), sahTraceZoneLevel(zone));
        parameter_setWriteHandler(param, pcb_sysbus_tracingParameterChanged);
        zone = sahTraceNextZone(zone);
    }

    // create system information
    system_object_info_t* info = (system_object_info_t*) calloc(1, sizeof(system_object_info_t));
    if(!info) {
        SAH_TRACE_ERROR("Failed to create system information");
    } else {
        info->type = system_object_self;
    }
    object_setUserData(instance, info);
    object_setDestroyHandler(instance, pcb_sysbus_objectDestroy);

    object_commit(instance);

    // remove mtk functions and parameters
    pcb_sysbus_processRemoveMtk(instance);

    object_commit(instance);

    self = instance;

    return true;
}
