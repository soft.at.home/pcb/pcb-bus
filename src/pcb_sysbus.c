/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

#include <components.h>

#include <debug/sahtrace.h>

#include <pcb/common/error.h>
#include <pcb/core.h>
#include <pcb/utils.h>

#include "parse_args.h"
#include "pcb_sysdatamodel.h"
#include "pcb_sysrequesthandlers.h"
#include "pcb_sysdispatching.h"
#include "pcb_sysprocesses.h"
#include "pcb_sysutils.h"
#include <pcb/utils/privilege.h>

extern application_config_t appconfig;
static bool exit_app = false;
object_t* self = NULL;

static request_handlers_t pcb_sysbus_RequestHandlers = {
    .getObject = pcb_sysbus_getHandler,
    .setObject = pcb_sysbus_defaultHandler,
    .createInstance = pcb_sysbus_defaultHandler,
    .deleteInstance = pcb_sysbus_defaultHandler,
    .findObjects = pcb_sysbus_findObjectsHandler,
    .executeFunction = pcb_sysbus_defaultHandler,
    .closeRequest = default_closeRequestHandler,
    .notify = NULL,
    .openSession = default_openSessionRequestHandler,
};

static void pcb_sysbus_signalHandler(int signal) {
    (void) signal;
    SAH_TRACE_NOTICE("Signal event handler called");
    exit_app = true;
}

static void pcb_sysbus_buildListenSocketList() {
    // initialize the lists
    // no need to check on failures here, both lists are allocated on the stack
    llist_initialize(&appconfig.listen_sockets);
    llist_initialize(&appconfig.interconnections);
    llist_initialize(&appconfig.sniffers);

    // take command line arguments and environment variables first
    const char* address = appconfig.ipc_socket_name;
    const char* tcp_address = appconfig.tcp_address;
    const char* tcp_port = appconfig.tcp_port;
    uri_t* uri = NULL;

    const char* URI = getenv("PCB_SYS_BUS");
    if(URI) {
        uri = uri_parse(URI);
        if(uri && (strcmp(uri_getScheme(uri), "pcb") != 0)) {
            SAH_TRACE_ERROR("PCB_SYS_BUS env variable does not contain a pcb URI - wrong scheme [%s]", URI);
            uri_destroy(uri);
            uri = NULL;
        }
    }

    if(!address || !(*address)) {
        // env variable PCB_SYSTEM is considered depricated
        // but still here for backwards compatibility
        address = getenv("PCB_SYSTEM");
    }

    if((!address || !(*address)) &&
       uri && (strcmp(uri_getHost(uri), "ipc") == 0)) {
        address = uri_getPort(uri);
    }

    if(!address || !(*address)) {
        address = "/var/run/pcb_sys";
    }

    // add the ipc listen sockets if available
    if(address && *address) {
        pcb_sysbus_addListenSocket("ipc", address, true, NULL, 0);
#ifdef CONFIG_SAH_SERVICES_PCB_HTTPD
        // add the httpd listen socket (for backward compatibility)
        char httpd[512];
        // build the new address
        strncpy(httpd, address, 500);
        httpd[strlen(httpd)] = 0;
        strncat(httpd, ".httpd", 500 - strlen(address));
        pcb_sysbus_addListenSocket("ipc", httpd, true, "httpd", UINT32_MAX);
#endif
    }

    // add tcp listen sockets if available
    if((!tcp_address || !tcp_port || !(*tcp_address) | !(*tcp_port)) &&
       uri && (strcmp(uri_getHost(uri), "ipc") != 0)) {
        tcp_address = uri_getHost(uri);
        tcp_port = uri_getPort(uri);
    }

    // TCP listen socket
    if(tcp_address && *tcp_address && tcp_port && *tcp_port) {
        pcb_sysbus_addListenSocket(tcp_address, tcp_port, false, NULL, 0);
    }

    uri_destroy(uri);
}

static bool sysbus_datamodel_load(datamodel_t* dm, const char* path) {
    return datamodel_load(dm, path, SERIALIZE_FORMAT(serialize_format_odl, 0, 0));
}

static bool pcb_sysbus_initialize() {
    // initialize pcb
    //////////////////////////////////////////
    object_t* bus_tracing = NULL;

    connection_enableSSL(false);
    connection_initLibrary();

    // create the connection object
    appconfig.pcb = pcb_create(appconfig.name, 0, NULL);
    if(!appconfig.pcb) {
        SAH_TRACE_ERROR("Failed to create connection object");
        goto error;
    }

    // set the request handlers (overwrite the defaults)
    pcb_setRequestHandlers(appconfig.pcb, &pcb_sysbus_RequestHandlers);

    // set the default format
    if(!pcb_setDefaultFormat(appconfig.pcb, SERIALIZE_FORMAT(serialize_format_default, SERIALIZE_DEFAULT_MAJOR, SERIALIZE_DEFAULT_MINOR))) {
        SAH_TRACE_ERROR("Failed to find serialization format");
        goto error_cleanup_connection;
    }

    if(!pcb_sysbus_listen()) {
        goto error_cleanup_connection;
    }

    // add full access for everyone
    // the bus does not known the access rights, so here everyone is allowed
    object_aclAdd(datamodel_root(pcb_datamodel(appconfig.pcb)), UINT32_MAX, acl_read | acl_write | acl_execute);

    // set signal event handlers
    connection_setSignalEventHandler(pcb_connection(appconfig.pcb), SIGINT, pcb_sysbus_signalHandler);
    connection_setSignalEventHandler(pcb_connection(appconfig.pcb), SIGTERM, pcb_sysbus_signalHandler);
    connection_setSignalEventHandler(pcb_connection(appconfig.pcb), SIGPIPE, NULL); // ignore sigpipe

    // set notify handler
    if(!pcb_setNotifyHandler(appconfig.pcb, pcb_sysbus_notifyHandler)) {
        SAH_TRACE_ERROR("Failed to set notify handler");
        goto error_cleanup_connection;
    }

    // create processes data model
    SAH_TRACE_INFO("Creating sysbus data model");
    pcb_sysbus_createDatamodel(appconfig.pcb);

    // set the namespace
    datamodel_setNamespace(pcb_datamodel(appconfig.pcb), appconfig.name);
    object_setSysbusRoot(datamodel_root(pcb_datamodel(appconfig.pcb)), true);

    SAH_TRACE_INFO("Load defaults if any available ...");

    datamodel_t* const dm = pcb_datamodel(appconfig.pcb);

    struct stat sb;
    if(stat(CONFIG_RWDATAPATH "/process-defaults.odl", &sb) == 0) {
        if(!sysbus_datamodel_load(dm, CONFIG_RWDATAPATH "/process-defaults.odl")) {
            SAH_TRACE_ERROR("Failed to load "CONFIG_RWDATAPATH "/process-defaults.odl (%m)");
        }
    }
    if(stat("/etc/config/process-defaults.odl", &sb) == 0) {
        if(!sysbus_datamodel_load(dm, "/etc/config/process-defaults.odl")) {
            SAH_TRACE_ERROR("Failed to load /etc/config/process-defaults.odl (%m)");
        }
    }

    bus_tracing = object_getObjectByKey(datamodel_root(pcb_datamodel(appconfig.pcb)), "Process.%s.Tracing", appconfig.name);
    pcb_sysbus_enableTraceZones(bus_tracing);

    SAH_TRACE_INFO("pcb_sysbus initialized");
    if(!appconfig.foreground) {
        SAH_TRACE_INFO("daemonize pcb sysbus");
        if(daemon(1, 1) == -1) {
            SAH_TRACE_ERROR("Failed to daemonize");
        }
    }

    pcb_sysbus_addself();

    return true;

error_cleanup_connection:
    pcb_destroy(appconfig.pcb);
    appconfig.pcb = NULL;

error:
    fprintf(stdout, "pcb sysbus failed to initialize\n");
    SAH_TRACE_ERROR("pcb sysbus failed to initialize");
    return false;
}

static int pcb_sysbus_eventloop() {
    int retval = 0;

    SAH_TRACE_NOTICE("%s entered event loop", appconfig.name);

    fd_set readset;
    fd_set writeset;

    llist_t cons;
    llist_initialize(&cons);
    llist_append(&cons, connection_getIterator(pcb_connection(appconfig.pcb)));

    while(!exit_app) {
        // call select
        retval = connection_waitForEvents_r(&cons, NULL, &readset, &writeset);
        if(retval < 0) {
            // an error has happened
            SAH_TRACE_ERROR("Error %d (%s)", pcb_error, error_string(pcb_error));
            continue;
        }

        if(retval >= 0) {
            // events available (signals or fd events)
            connection_handleEvents_r(&cons, &readset, &writeset);
        }
    }

    llist_cleanup(&cons);
    SAH_TRACE_NOTICE("%s leaving event loop", appconfig.name);
    return retval;
}

static void pcb_sysbus_cleanup() {
    llist_iterator_t* it = llist_takeFirst(&appconfig.listen_sockets);
    listen_socket_t* socket_info = NULL;

    while(it) {
        socket_info = llist_item_data(it, listen_socket_t, it);
        free(socket_info->address);
        free(socket_info->port);
        free(socket_info->username);
        free(socket_info);
        it = llist_takeFirst(&appconfig.listen_sockets);
    }

    self = NULL;
    pcb_destroy(appconfig.pcb);
    connection_exitLibrary();
}

int main(int argc, char* argv[]) {
    int retval = 0;
    bool createPid = false;
    char* pidfile = NULL;

    // parse command line arguments
    int i = parse_arguments(argc, argv);
    if(i == 0) {
        return 0;
    }

    // initialize tracing and logging
    sahTraceOpen(appconfig.name, appconfig.trace_output);
    sahTraceSetLevel(appconfig.trace_level);
    SAH_TRACE_INFO("Configuration flags = 0x%8X", appconfig.config_flags);

    const bool can_use_privileges = priv_proc_initialize();
    if(!can_use_privileges) {
        SAH_TRACE_ERROR("Failed to initialize privilege library");
    }

    // change the application name
    createPid = pcb_sysbus_setAppname(argv[0]);

    // build list of listen sockets and bus interconnection sockets
    pcb_sysbus_buildListenSocketList();

    // initialize
    if(!pcb_sysbus_initialize()) {
        SAH_TRACE_ERROR("Initialization failed");
        goto error_exit;
    }

    // create pid file, if needed
    if(createPid) {
        pidfile = pcb_sysbus_createPidFile();
    }

    if(can_use_privileges) {
        priv_proc_dropPrivileges(
            priv_get_defaultUser(),
            priv_get_defaultGroup(),
            priv_get_defaultRetain()
            );
    } else {
        SAH_TRACE_ERROR("Not dropping privileges; is that the intent?");
    }
    // all initialization done,
    // start the eventloop
    SAH_TRACE_INFO("Started");
    retval = pcb_sysbus_eventloop();
    SAH_TRACE_INFO("Stopping");
    if(can_use_privileges) {
        priv_proc_cleanup();
    }

    // remove the pid file
    if(createPid && pidfile) {
        unlink(pidfile);
    }
    free(pidfile);

    // cleanup
    pcb_sysbus_cleanup();

    sahTraceClose();
    return retval;

error_exit:
    pcb_sysbus_cleanup();
    // close traces
    sahTraceClose();
    return -1;
}
