/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#include <components.h>

#include <debug/sahtrace.h>

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#endif

#include <pcb/common/error.h>
#include <pcb/core.h>
#include <pcb/utils.h>

#include "parse_args.h"
#include "pcb_sysdatamodel.h"
#include "pcb_sysrequesthandlers.h"
#include "pcb_sysdispatching.h"
#include "pcb_sysprocesses.h"
#include "pcb_sysutils.h"

extern application_config_t appconfig;

static bool pcb_sysbus_accept(peer_info_t* peer) {
    peer_info_t* listensocket = peer_listenSocket(peer);

    listen_socket_t* info = peer_getUserData(listensocket);

    // close all fd forwarded
    peer_setCloseFd(peer, true);

    // set peer trusted flag
    pcb_setTrusted(appconfig.pcb, peer, info->trusted);
    // set default user id
    SAH_TRACE_INFO("Setting default user id %d", info->defaultUid);
    pcb_setDefaultUid(appconfig.pcb, peer, info->defaultUid);

    if(peer_isIPCSocket(peer)) {
        return true;
    }

    // Only done for TCP sockets
    // set the keep alive options and enable keep alive
    pcb_sysbus_setTCPKeepAlive(peer);

    return true;
}

static bool pcb_sysbus_changeOwner(const char* port, const char* username) {
    if(username) {
        // fetch the user id
        struct passwd* pd;
        if((pd = getpwnam(username)) == NULL) {
            SAH_TRACE_ERROR("User \"%s\" not found", username);
            return false;
        }

        // set the access rights
        if(chown(port, pd->pw_uid, 0) == -1) {
            SAH_TRACE_ERROR("Failed change owner (%d)", errno);
            return false;
        }

        // set the access rights
        if(chmod(port, S_IRUSR | S_IWUSR) == -1) {
            SAH_TRACE_ERROR("Failed change permissions (%d)", errno);
            return false;
        }
    } else {
        struct group* grp = getgrnam("pcb_users");
        if(grp != NULL) {
            // if group 'pcb_users' exists, restrict access to root and this group
            if(chown(port, 0, grp->gr_gid) == -1) {
                SAH_TRACE_ERROR("Failed change group (%d)", errno);
                return false;
            }
            if(chmod(port, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP) == -1) {
                SAH_TRACE_ERROR("Failed change permissions (%d)", errno);
                return false;
            }
        } else {
            // give everybody access rights
            if(chmod(port, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH) == -1) {
                SAH_TRACE_ERROR("Failed change permissions (%d)", errno);
                return false;
            }
        }
    }

    return true;
}

bool pcb_sysbus_setAppname(char* argv0) {
    bool createPid = false;

    // change the name of the application
    if(*appconfig.name) {
        char name[16] = "";
        strncpy(name, appconfig.name, 15);
        name[strlen(name)] = 0;
        strncpy(argv0, name, strlen(argv0));
        if(prctl(PR_SET_NAME, (unsigned long) name, 0, 0, 0) < 0) {
            SAH_TRACE_ERROR("unable to change name: %d", errno);
        }
    }

    // if no name was specified, use the application name
    if(!*appconfig.name) {
        appconfig.name = argv0;
    } else if(!appconfig.foreground) {
        createPid = true;
    }

    return createPid;
}

char* pcb_sysbus_createPidFile() {
    pid_t pid = getpid();
    // set the nice level
    if(setpriority(PRIO_PROCESS, pid, appconfig.priority) == -1) {
        SAH_TRACE_ERROR("Failed to set nice level %d", errno);
    }

    string_t pidfile;
    string_initialize(&pidfile, 64);

    // create pidfile
    string_initialize(&pidfile, 64);
    string_fromChar(&pidfile, "/var/run/");
    string_appendChar(&pidfile, appconfig.name);
    string_appendChar(&pidfile, ".pid");
    FILE* pf = fopen(string_buffer(&pidfile), "w");
    if(pf) {
        fprintf(pf, "%d", pid);
        fflush(pf);
        fclose(pf);
    } else {
        SAH_TRACE_ERROR("Failed to create pidfile for %s", appconfig.name);
    }

    return (char*) string_buffer(&pidfile);
}

#ifdef CONFIG_PCB_ACL_USERMNGT
uint32_t pcb_sysbus_getGroupId(const char* name) {
    uint32_t id = 0;
    const usermngt_group_t* gr = usermngt_groupFindByName(name);
    if(gr == NULL) {
        SAH_TRACE_WARNING("unable to fetch id for group %s", name);
        return UINT32_MAX;
    }
    SAH_TRACE_INFO("Group %s: id = %d", name, usermngt_groupID(gr));
    id = usermngt_groupID(gr);

    return id;
}
#endif

void pcb_sysbus_setTCPKeepAlive(peer_info_t* peer) {
    int fd = peer_getFd(peer);
    if(fd >= 0) {
        int optval = 1;
        socklen_t optlen = sizeof(optval);
        if(setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
            SAH_TRACE_ERROR("Failed to enable keep alive");
        }

        optval = 60;
        if(setsockopt(fd, SOL_TCP, TCP_KEEPIDLE, &optval, optlen) < 0) {
            SAH_TRACE_ERROR("Failed to set idle time");
        }

        optval = 30;
        if(setsockopt(fd, SOL_TCP, TCP_KEEPINTVL, &optval, optlen) < 0) {
            SAH_TRACE_ERROR("Failed to set interval");
        }

        optval = 3;
        if(setsockopt(fd, SOL_TCP, TCP_KEEPCNT, &optval, optlen) < 0) {
            SAH_TRACE_ERROR("Failed to set max count");
        }
    }
}

listen_socket_t* pcb_sysbus_addListenSocket(const char* address, const char* port, bool trusted, const char* username, uint32_t userId) {
    listen_socket_t* socket_info = NULL;
    if(!address || !port) {
        goto exit_error;
    }

    socket_info = (listen_socket_t*) calloc(1, sizeof(listen_socket_t));
    if(!socket_info) {
        SAH_TRACE_ERROR("Failed to allocate memory for listen socket information");
        SAH_TRACE_ERROR("pcb://[%s]:[%s] not added", address, port);
        goto exit_error;
    }

    socket_info->address = strdup(address);
    if(!socket_info->address) {
        SAH_TRACE_ERROR("Failed to allocate memory");
        goto exit_free_socket_info;
    }
    socket_info->port = strdup(port);
    if(!socket_info->port) {
        SAH_TRACE_ERROR("Failed to allocate memory");
        goto exit_free_address;
    }
    if(username) {
        socket_info->username = strdup(username);
        if(!socket_info->port) {
            SAH_TRACE_ERROR("Failed to allocate memory");
            goto exit_free_port;
        }
    }

    socket_info->defaultUid = userId;
    socket_info->trusted = trusted;

    SAH_TRACE_INFO("Add listen socket \"pcb://[%s]:[%s]\", trusted = %s, defaultUid = %d", socket_info->address, socket_info->port, socket_info->trusted ? "Yes" : "No", socket_info->defaultUid);
    llist_append(&appconfig.listen_sockets, &socket_info->it);
    return socket_info;

exit_free_port:
    free(socket_info->port);
exit_free_address:
    free(socket_info->address);
exit_free_socket_info:
    free(socket_info);
exit_error:
    return NULL;
}

bool pcb_sysbus_removeListenSocket(const char* address, const char* port) {
    llist_iterator_t* it = NULL;
    listen_socket_t* info = NULL;

    llist_for_each(it, &appconfig.listen_sockets) {
        info = llist_item_data(it, listen_socket_t, it);
        if(address && (strcmp(info->address, address) == 0) &&
           ((!port && !info->port) || (info->port && port && (strcmp(info->port, port) == 0)))) {
            llist_iterator_take(it);
            peer_destroy(info->peer);
            free(info->port);
            free(info->address);
            free(info);
            return true;
        }
    }

    return false;
}

peer_info_t* pcb_sysbus_createListenSocket(listen_socket_t* info) {
    uint32_t flags = connection_attr_default;

    if(strcmp(info->address, "ipc") == 0) {
        SAH_TRACE_INFO("Start listening on IPC socket %s", info->port);
        info->peer = connection_listenOnIPC(pcb_connection(appconfig.pcb), info->port);
        if(info->peer) {
            pcb_sysbus_changeOwner(info->port, info->username);
        }
    } else {
        SAH_TRACE_INFO("Start listening on TCP socket %s:%s", info->address, info->port);
        info->peer = connection_listenOnTCP(pcb_connection(appconfig.pcb), info->address, info->port, flags);
    }
    if(!info->peer) {
        SAH_TRACE_ERROR("Failed to create listen socket pcb://[%s]:[%s] - %d", info->address, info->port, errno);
        return NULL;
    }

    peer_addAcceptHandler(info->peer, pcb_sysbus_accept);
    peer_setUserData(info->peer, info);
    return info->peer;
}

bool pcb_sysbus_listen() {
    llist_iterator_t* it = NULL;
    listen_socket_t* info = NULL;
    uint32_t listen = 0;

    llist_for_each(it, &appconfig.listen_sockets) {
        info = llist_item_data(it, listen_socket_t, it);
        if(pcb_sysbus_createListenSocket(info)) {
            listen++;
        }
    }

    // Check that at least one listen socket is available, otherwise just exit the application
    if(!listen) {
        SAH_TRACE_ERROR("No listen socket(s) available, stop");
        return false;
    }

    return true;
}

notification_t* pcb_sysbus_createApplicationStarted(object_t* root, object_t* proc) {
    notification_t* notification = notification_create(notify_application_started);
    if(!notification) {
        return NULL;
    }

    notification_setName(notification, "application_started");

    // add the notification parameters
    char* value = NULL;
    notification_parameter_t* notif_param = notification_parameter_create("Application", object_da_parameterCharValue(proc, "Name"));
    notification_addParameter(notification, notif_param);

    value = object_parameterCharValue(proc, "PID");
    notif_param = notification_parameter_create("PID", value);
    notification_addParameter(notification, notif_param);
    free(value);

    value = object_parameterCharValue(proc, "Namespace");
    if(value && *value) {
        notif_param = notification_parameter_create("Namespace", value);
        notification_addParameter(notification, notif_param);
    }
    free(value);

    notif_param = notification_parameter_create("BusName", object_name(proc, path_attr_key_notation));
    notification_addParameter(notification, notif_param);

    // trace zones will not be added

    // add all dispatch object for this process to the notification
    object_t* dispatcher = NULL;
    system_object_info_t* info = NULL;
    object_for_each_child(dispatcher, root) {
        info = object_getUserData(dispatcher);
        if(!info ||
           (info->type == system_object_internal) ||
           (info->type == system_object_self)) {
            continue;
        }
        if(info->process == proc) {
            notif_param = notification_parameter_create("Object", object_name(dispatcher, path_attr_key_notation));
            notification_addParameter(notification, notif_param);
        }
    }

    return notification;
}

notification_t* pcb_sysbus_createApplicationStopping(object_t* root, object_t* proc) {
    notification_t* stop = notification_createApplicationStopping(object_da_parameterCharValue(proc, "Name"));
    if(!stop) {
        return NULL;
    }

    notification_parameter_t* notif_param = notification_parameter_create("BusName", object_name(proc, path_attr_key_notation));
    notification_addParameter(stop, notif_param);

    object_t* dispatcher = NULL;
    system_object_info_t* info = NULL;
    object_for_each_child(dispatcher, root) {
        info = object_getUserData(dispatcher);
        if(!info ||
           (info->type == system_object_internal) ||
           (info->type == system_object_self)) {
            continue;
        }
        if(info->process == proc) {
            notification_parameter_t* notif_param_proc = notification_parameter_create("Object", object_name(dispatcher, path_attr_key_notation));
            notification_addParameter(stop, notif_param_proc);
            info->peer = NULL;
            info->process = NULL;
            object_delete(dispatcher);
        }
    }

    return stop;
}

const char* print_request_type_name(request_t* req) {
    const char* request_type_name = "";
    switch(request_type(req)) {
    case request_type_get_object:      request_type_name = "GET"; break;
    case request_type_set_object:      request_type_name = "SET"; break;
    case request_type_create_instance: request_type_name = "CREATE"; break;
    case request_type_delete_instance: request_type_name = "DELETE"; break;
    case request_type_find_objects:    request_type_name = "FIND"; break;
    case request_type_exec_function:   request_type_name = "EXEC"; break;
    case request_type_close_request:   request_type_name = "CLOSE"; break;
    default:
        request_type_name = "UNKNOWN";
        break;
    }
    return request_type_name;
}

void pcb_sysbus_sniffer(request_t* req) {
    uint32_t pid = request_getPid(req);
    uint32_t* active_pid = NULL;
    bool dbg_req = appconfig.sniff;
    variant_list_for_each_uint32(active_pid, &appconfig.sniffers) {
        if(*active_pid == pid) {
            dbg_req = true;
        }
    }

    if(!dbg_req) {
        return;
    }

    SAH_TRACEZ_INFO("sniffer", "============================");
    SAH_TRACEZ_INFO("sniffer", "Request type   = [%d] - %s", request_type(req), print_request_type_name(req));
    SAH_TRACEZ_INFO("sniffer", "Request attrib = [0x%X]", request_attributes(req));
    SAH_TRACEZ_INFO("sniffer", "User ID        = [%d]", request_userID(req));
    SAH_TRACEZ_INFO("sniffer", "Source PID     = [%d]", request_getPid(req));
    SAH_TRACEZ_INFO("sniffer", "Object path    = [%s]", request_path(req));
    switch(request_type(req)) {
    case request_type_get_object:
        SAH_TRACEZ_INFO("sniffer", "    depth          = [%d]", request_depth(req));
        break;
    case request_type_set_object:
        SAH_TRACEZ_INFO("sniffer", "    Nr. Of Params  = [%d]", llist_size(request_parameterList(req)));
        break;
    case request_type_create_instance:
        SAH_TRACEZ_INFO("sniffer", "    Index          = [%d]", request_instanceIndex(req));
        SAH_TRACEZ_INFO("sniffer", "    Key            = [%s]", request_instanceKey(req));
        break;
    case request_type_find_objects:
        SAH_TRACEZ_INFO("sniffer", "    depth          = [%d]", request_depth(req));
        SAH_TRACEZ_INFO("sniffer", "    Pattern        = [%s]", request_pattern(req));
        break;
    case request_type_exec_function:
        SAH_TRACEZ_INFO("sniffer", "    Function name  = [%s]", request_functionName(req));
        SAH_TRACEZ_INFO("sniffer", "    Nr. Of Args    = [%d]", llist_size(request_parameterList(req)));
        break;
    default:
        break;
    }
    SAH_TRACEZ_INFO("sniffer", "============================");
}
