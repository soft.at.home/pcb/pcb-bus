/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <components.h>
#include <assert.h>
#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#endif

#include <pcb/core/types.h>
#include <pcb/core/notification.h>

#include "parse_args.h"
#include "pcb_sysdatamodel.h"
#include "pcb_sysdispatching.h"
#include "pcb_sysutils.h"
#include "pcb_sysbus_mtk.h"

extern application_config_t appconfig;

#define DECIMAL_DIGITS_PER_BYTE 3

#ifdef CONFIG_PCB_SYSBUS_LXC_SUPPORT
#include <lxc/lxccontainer.h>

static bool isRunningContainer(object_t* instance) {
    if(!instance) {
        return false;
    }
    char* name = object_parameterCharValue(instance, "Name");
    if(strlen(name) == 0) {
        free(name);
        return false;
    }
    bool found = false;

    char** running;
    int i = list_active_containers(CONFIG_SAH_SERVICES_LXC_PATH, &running, NULL);
    while(i > 0) {
        --i;
        if(strncmp(name, running[i], strlen(name)) == 0) {
            SAH_TRACE_INFO("found running container %s", name);
            found = true;
        }
        free(running[i]);
    }
    free(name);
    return found;
}
#endif

typedef enum process_exists_t {
    PROC_EXISTS_DONTKNOW,
    PROC_EXISTS_YES,
    PROC_EXISTS_NO
} process_exists_t;

static process_exists_t process_exists(pid_t pid) {
#ifdef CONFIG_PCB_SYSBUS_LXC_SUPPORT
    (void) pid;
    return PROC_EXISTS_DONTKNOW;
#else
    if((kill(pid, 0) == 0) || (errno == EPERM)) {
        return PROC_EXISTS_YES;
    } else {
        SAH_TRACE_NOTICE("Failed to validate existence of PID %d (%m)", pid);
        if(errno == ESRCH) {
            return PROC_EXISTS_NO;
        } else {
            return PROC_EXISTS_DONTKNOW;
        }
    }
#endif
}

static void pcb_sysbus_verifyProcesses(object_t* processes) {
    pcb_t* pcb = datamodel_pcb(object_datamodel(processes));
    if(!pcb) {
        SAH_TRACE_OUT();
        return;
    }

    object_t* instance = object_firstInstance(processes);
    object_t* prefetch = NULL;
    while(instance) {
        prefetch = object_nextInstance(instance);
        system_object_info_t* info = (system_object_info_t*) object_getUserData(instance);
        // skip processes without internal information
        if(!info) {
            instance = prefetch;
            continue;
        }
        // skip processes connected using TCP socket
        if(peer_isTCPSocket(info->peer)) {
            instance = prefetch;
            continue;
        }
        // skip processes with parent process
        if(info->process) {
            instance = prefetch;
            continue;
        }
#ifdef CONFIG_PCB_SYSBUS_LXC_SUPPORT
        if(isRunningContainer(instance)) {
            instance = prefetch;
            continue;
        }
#endif
        pid_t pid = object_parameterInt32Value(instance, "PID");

        switch(process_exists(pid)) {
        case PROC_EXISTS_DONTKNOW:
#ifndef CONFIG_PCB_SYSBUS_LXC_SUPPORT
            SAH_TRACE_ERROR("Can't verify if process [%d] exists (%m)", (int) pid);
#endif
            break;
        case PROC_EXISTS_YES:
            break;
        case PROC_EXISTS_NO:
            SAH_TRACE_INFO("Process is gone, destroy peer");
            peer_destroy(info->peer);
            break;
        }

        instance = prefetch;
    }
    object_commit(processes);
}

static void pcb_sysbus_resetInterconnected(peer_info_t* peer, object_t* busproc) {
    connection_info_t* con = peer_connection(peer);
    pcb_t* pcb = pcb_fromConnection(con);
    if(!pcb) {
        return;
    }

    // remove the processes
    object_t* processes = pcb_getObject(pcb, "Process", path_attr_default);
    if(!processes) {
        SAH_TRACE_OUT();
        return;
    }

    object_t* instance = NULL;
    object_t* root = datamodel_root(pcb_datamodel(pcb));
    object_for_each_instance(instance, processes) {
        system_object_info_t* info = (system_object_info_t*) object_getUserData(instance);
        if(!info || !info->process) {
            continue;
        }

        if(info->process == busproc) {
            SAH_TRACE_INFO("Reset interconnected process (%s)", object_name(instance, path_attr_key_notation));
            pcb_sysbus_processReset(instance, "Disconnected");
            // reset process peer to NULL
            info->peer = NULL;
            // remove all dispatch objects
            object_t* dispatch = NULL;
            object_for_each_child(dispatch, root) {
                system_object_info_t* object_info = (system_object_info_t*) object_getUserData(dispatch);
                if(!object_info) {
                    continue;
                }

                if(object_info->process == instance) {
                    SAH_TRACE_NOTICE("Removing disaptch object %s", object_name(dispatch, path_attr_key_notation));
                    object_delete(dispatch);
                }
            }
            // remove process
            object_delete(instance);
        }
    }

    object_commit(root);
}

static bool pcb_sysbus_peerClosed(peer_info_t* peer) {
    SAH_TRACE_IN();

    connection_info_t* con = peer_connection(peer);
    pcb_t* pcb = pcb_fromConnection(con);
    if(!pcb) {
        SAH_TRACE_OUT();
        return false;
    }

    // remove the processes
    object_t* processes = pcb_getObject(pcb, "Process", path_attr_default);
    if(!processes) {
        SAH_TRACE_OUT();
        return false;
    }

    object_t* instance = NULL;
    object_t* root = datamodel_root(pcb_datamodel(pcb));
    object_for_each_instance(instance, processes) {
        system_object_info_t* info = (system_object_info_t*) object_getUserData(instance);
        if(!info || !info->peer) {
            continue;
        }

        if(info->peer != peer) {
            continue;
        }

        if(strcmp(object_da_parameterCharValue(instance, "Status"), "Stopped") != 0) {
            // Create application stopping notification and send it
            notification_t* stop = pcb_sysbus_createApplicationStopping(root, instance);
            SAH_TRACE_INFO("Send stopping notification for process: %s", object_name(instance, path_attr_key_notation));
            pcb_sendNotification(pcb, NULL, info->peer, stop);
            notification_destroy(stop);
        }

        // reset processes information
        pcb_sysbus_processReset(instance, "Disconnected");

        info->peer = NULL;
        if(info->process) {
            // remove interconnected processes
            SAH_TRACE_INFO("Removing interconnected process (%s)", object_name(instance, path_attr_key_notation));
            object_delete(instance);
        }
    }

    object_commit(root);

    SAH_TRACE_OUT();
    return true;
}

static void pcb_sysbus_changeTraceZones(parameter_t* parameter) {
    const char* name = parameter_name(parameter);
    const variant_t* value = parameter_getValue(parameter);

    if(strcmp(name, "TraceLevel") == 0) {
        appconfig.trace_level = variant_uint32(value);
        SAH_TRACE_INFO("Change TraceLevel to %d", appconfig.trace_level);
        sahTraceSetLevel(appconfig.trace_level);
    } else if(strcmp(name, "Enabled") == 0) {
        SAH_TRACE_INFO("%s tracing", variant_bool(value) ? "Enable" : "Disable");
        if(variant_bool(value)) {
            sahTraceOpen(appconfig.name, appconfig.trace_output);
            // enable trace zones
            pcb_sysbus_enableTraceZones(parameter_owner(parameter));
        } else {
            sahTraceClose();
        }
    } else {
        sah_trace_zone_it* zone = sahTraceGetZone(name);
        if(zone) {
            sahTraceZoneSetLevel(zone, variant_uint32(value));
        } else {
            sahTraceAddZone(variant_uint32(value), name);
        }
    }
}

static system_object_info_t* pcb_sysbus_processAllocateInfo(object_t* process) {
    system_object_info_t* info = (system_object_info_t*) calloc(1, sizeof(system_object_info_t));
    if(!info) {
        SAH_TRACE_ERROR("Failed to allocate info object");
        return NULL;
    }

    object_setUserData(process, info);
    object_setDestroyHandler(process, pcb_sysbus_objectDestroy);

    return info;
}

static object_t* pcb_sysbus_processAllocate(object_t* processes, const char* proc_name) {
    object_t* process = object_createInstance(processes, 0, proc_name);
    if(!process) {
        SAH_TRACE_ERROR("Failed to create process (%s)", error_string(pcb_error));
        return NULL;
    }

    if(!pcb_sysbus_processAllocateInfo(process)) {
        object_rollback(process);
        process = NULL;
    }

    return process;
}

static bool pcb_sysbus_setProcessData(object_t* process, peer_info_t* peer, notification_t* notification, bool setTraces) {
    notification_parameter_t* param_name = notification_getParameter(notification, "Application");
    notification_parameter_t* param_pid = notification_getParameter(notification, "PID");
    notification_parameter_t* param_trace = notification_getParameter(notification, "TraceLevel");
    notification_parameter_t* param_ns = notification_getParameter(notification, "Namespace");
    notification_parameter_t* param_bus_name = notification_getParameter(notification, "BusName");

    if(!param_bus_name && notification) {
        param_bus_name = notification_parameter_create("BusName", object_name(process, path_attr_key_notation));
        notification_addParameter(notification, param_bus_name);
    }

    char* value = notification_parameter_value(param_pid);
    if(value) {
        object_parameterSetCharValue(process, "PID", value);
        free(value);
    }

    object_parameterSetBoolValue(process, "Registered", true);

    value = notification_parameter_value(param_name);
    object_parameterSetCharValue(process, "Name", value);
    free(value);

    value = notification_parameter_value(param_ns);
    object_parameterSetCharValue(process, "Namespace", value);
    free(value);

    object_parameterSetCharValue(process, "Status", "Started");

    if(notification_type(notification) == notify_application_started) {
        object_parameterSetCharValue(process, "Type", "Client");
    } else {
        object_parameterSetCharValue(process, "Type", "Bus");
    }

    if(peer_isIPCSocket(peer)) {
        object_parameterSetCharValue(process, "Connection", "IPC");
    } else {
        object_parameterSetCharValue(process, "Connection", "TCP");
    }

    if(setTraces) {
        SAH_TRACE_INFO("Apply tracing info from appplication");
        object_t* traces = object_getObject(process, "Tracing", path_attr_default, NULL);

        value = notification_parameter_value(param_trace);
        object_parameterSetCharValue(traces, "TraceLevel", value);
        free(value);

        object_parameterSetBoolValue(traces, "Enabled", true);

        notification_parameter_t* notif_param = notification_firstParameter(notification);
        while(notif_param) {
            // skip object names
            if(strcmp(notification_parameter_name(notif_param), "Object") == 0) {
                notif_param = notification_nextParameter(notif_param);
                continue;
            }

            // skip fixed (or known) parameters
            if((notif_param == param_name) ||
               (notif_param == param_pid) ||
               (notif_param == param_trace) ||
               (notif_param == param_ns) ||
               (notif_param == param_bus_name)) {
                notif_param = notification_nextParameter(notif_param);
                continue;
            }

            // create additional parameter
            parameter_t* param = parameter_create(traces, notification_parameter_name(notif_param), parameter_type_uint32, parameter_attr_persistent);
            char* parameter_value = notification_parameter_value(notif_param);
            parameter_setFromChar(param, parameter_value);
            free(parameter_value);
            parameter_commit(param);
            parameter_setWriteHandler(param, pcb_sysbus_tracingParameterChanged);

            notif_param = notification_nextParameter(notif_param);
        }
    } else {
        object_t* traces = object_getObjectByKey(process, "Tracing");
        if(traces) {
            SAH_TRACE_INFO("Send tracing info to appplication");
            parameter_t* parameter = NULL;
            object_for_each_parameter(parameter, traces) {
                parameter_setWriteHandler(parameter, pcb_sysbus_tracingParameterChanged);
            }
            parameter = object_getParameter(traces, "Enabled");
            pcb_sysbus_tracingParameterChanged(parameter, NULL);
        }
    }

    object_commit(process);

    return true;
}

object_t* pcb_sysbus_createProcess(pcb_t* pcb, peer_info_t* peer, notification_t* notification, object_t* interconnected, const char* name) {
    object_t* process = NULL;
    system_object_info_t* info = NULL;
    bool persistent = true;

    object_t* processes = NULL;
    char* proc_name = NULL;
    char* app_name = NULL;
    uint32_t index = 0;

    processes = pcb_getObject(pcb, "Process", path_attr_default);

    if(notification) {
        notification_parameter_t* param_name = notification_getParameter(notification, "Application");
        notification_parameter_t* param_bus_name = notification_getParameter(notification, "BusName");

        app_name = notification_parameter_value(param_name);

        if(interconnected || (notification_type(notification) == notify_bus_interconnect)) {
            persistent = false;
            if(param_bus_name) {
                proc_name = notification_parameter_value(param_bus_name);
            } else {
                proc_name = strdup(app_name);
            }
            if(!proc_name) {
                goto exit;
            }
            SAH_TRACE_INFO("Remote process: %s (Type = %s)", proc_name, (notification_type(notification) == notify_bus_interconnect) ? "Bus" : "App");
            process = object_getObjectByKey(processes, proc_name);
            if(!process) {
                SAH_TRACE_INFO("==> New process %s", proc_name);
                goto create_process;
            } else {
                SAH_TRACE_INFO("==> Known process %s", proc_name);
                goto leave;
            }
        } else {
            if(asprintf(&proc_name, "%s_%s", appconfig.name, app_name) < 0) {
                SAH_TRACE_ERROR("Failed to allocate memory for string");
                goto exit;
            }
        }
    } else {
        app_name = strdup(name);
        if(asprintf(&proc_name, "%s_%s", appconfig.name, app_name) < 0) {
            SAH_TRACE_ERROR("Failed to allocate memory for string");
            goto exit;
        }
    }

    process = object_getObjectByKey(processes, proc_name);
    if(!process) {
        goto create_process;
    } else {
        if(!object_parameterBoolValue(process, "Registered")) {
            SAH_TRACE_INFO("Re-use process %s", object_name(process, path_attr_key_notation));
            goto leave;
        }
    }
    free(proc_name);
    proc_name = NULL;

    SAH_TRACE_INFO("Adding index to process name ...");
    index = 1;
    do {
        if(asprintf(&proc_name, "%s_%s_%d", appconfig.name, app_name, index) < 0) {
            SAH_TRACE_ERROR("Failed to allocate memory for string");
            goto leave;
        }
        process = object_getObjectByKey(processes, proc_name);
        index++;
    } while(process && object_parameterBoolValue(process, "Registered"));

    if(process) {
        SAH_TRACE_INFO("Re-use process %s", object_name(process, path_attr_key_notation));
        goto leave;
    }

create_process:
    // create new process
    SAH_TRACE_INFO("Create new process [%s]", proc_name);
    process = pcb_sysbus_processAllocate(processes, proc_name);
    if(!process) {
        goto exit;
    }

    if(interconnected) {
        // process is not managed by this bus, but by another one
        // remove management data - (SharedObjects, Tracing, EnableSync and functions)
        object_t* so = object_getObjectByKey(process, "SharedObject");
        object_t* tr = object_getObjectByKey(process, "Tracing");
        parameter_t* sync = object_getParameter(process, "EnableSync");
        object_rollback(so);
        object_rollback(tr);
        parameter_rollback(sync);

        function_t* func = object_firstFunction(process);
        while(func) {
            function_delete(func);
            func = object_firstFunction(process);
        }
    }

    object_setPersistent(process, persistent);
    SAH_TRACE_INFO("Created process %s", object_name(process, path_attr_key_notation));

leave:
    info = object_getUserData(process);
    if(!info) {
        // when the processes were loaded from a file, they will not contain an info object
        info = pcb_sysbus_processAllocateInfo(process);
    } else {
        if(info->peer && (info->peer != peer) && (notification_type(notification) == notify_bus_interconnect)) {
            peer_info_t* old = info->peer;
            SAH_TRACE_NOTICE("Interconnected bus (%s) changed socket for process", proc_name);
            pcb_sysbus_resetInterconnected(peer, process);
            info->peer = NULL;
            peer_destroy(old);
        }
    }
    // set information part
    info->type = system_object_internal;
    info->peer = peer;
    info->process = interconnected;

    peer_addCloseHandler(peer, pcb_sysbus_peerClosed);

exit:
    free(proc_name);
    free(app_name);
    return process;
}

static function_exec_state_t pcb_sysbus_processesSave(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    function_exec_state_t fstate = function_exec_error;
    uint32_t attr = request_attributes(fcall_request(fcall));

    char* filename = NULL;
    // take arguments
    argument_getChar(&filename, args, attr, "fileName", CONFIG_RWDATAPATH "/process-defaults.odl");

    if(!datamodel_save(fcall_object(fcall), (uint32_t) -1, filename, SERIALIZE_FORMAT(serialize_format_odl, 0, 0))) {
        reply_addError(request_reply(fcall_request(fcall)), pcb_error, error_string(pcb_error), NULL);
        SAH_TRACE_WARNING("Failed to save data model - 0x%x(%s)", pcb_error, error_string(pcb_error));
        variant_setBool(retval, false);
        goto leave;
    }

    variant_setBool(retval, true);
    fstate = function_exec_done;
leave:

    free(filename);
    fcall_destroy(fcall);
    return fstate;
}

static function_exec_state_t pcb_sysbus_processesLoad(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    function_exec_state_t fstate = function_exec_error;
    uint32_t attr = request_attributes(fcall_request(fcall));
    object_t* bus_tracing = NULL;
    char* filename = NULL;
    // take arguments
    argument_getChar(&filename, args, attr, "fileName", CONFIG_RWDATAPATH "/process-defaults.odl");

    if(!datamodel_load(object_datamodel(fcall_object(fcall)), filename, SERIALIZE_FORMAT(serialize_format_odl, 0, 0))) {
        reply_addError(request_reply(fcall_request(fcall)), pcb_error, error_string(pcb_error), NULL);
        SAH_TRACE_WARNING("Failed to load data model - 0x%x(%s)", pcb_error, error_string(pcb_error));
        variant_setBool(retval, false);
        goto leave;
    }

    // fetch the sysbus process object and update trace level and zones
    bus_tracing = object_getObjectByKey(object_root(fcall_object(fcall)), "Process.%s.Tracing", appconfig.name);
    pcb_sysbus_enableTraceZones(bus_tracing);

    variant_setBool(retval, true);
    fstate = function_exec_done;
leave:

    free(filename);
    fcall_destroy(fcall);
    return fstate;
}

static void defered_peer_destroy(void* userdata) {
    system_object_info_t* info = (system_object_info_t*) userdata;
    peer_destroy(info->peer);
    info->peer = NULL;
}

static bool pcb_sysbus_onDeleteInstance(object_t* object, object_t* instance) {
    (void) object;
    if(object_parameterBoolValue(instance, "Registered")) {
        SAH_TRACE_INFO("Process %s is registered, can not be deleted", object_name(instance, path_attr_key_notation));
        return false;
    }

    return true;
}

static object_t* pcb_sysbus_createProcessesObject(object_t* root) {
    // Create Processes object
    object_t* processes = object_create(root, "Process", object_attr_template | object_attr_persistent);
    if(!processes) {
        SAH_TRACE_ERROR("Failed to create object 'Processes'");
        goto exit;
    }

    if(!object_setInstanceDelHandler(processes, pcb_sysbus_onDeleteInstance)) {
        SAH_TRACE_ERROR("Failed to add instance delete handler");
        goto exit_rollback;
    }

    if(!parameter_create(processes, "Name", parameter_type_string, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Name'");
        processes = NULL;
        goto exit_rollback;
    }

    if(!parameter_create(processes, "Registered", parameter_type_bool, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Registered'");
        processes = NULL;
        goto exit_rollback;
    }

    if(!parameter_create(processes, "PID", parameter_type_int32, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Pid'");
        processes = NULL;
        goto exit_rollback;
    }

    if(!parameter_create(processes, "Namespace", parameter_type_string, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Namespace'");
        processes = NULL;
        goto exit_rollback;
    }

    if(!parameter_create(processes, "Status", parameter_type_string, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Status'");
        processes = NULL;
        goto exit_rollback;
    }
    object_parameterSetCharValue(processes, "Status", "Disconnected");

    if(!parameter_create(processes, "Type", parameter_type_string, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Type'");
        processes = NULL;
        goto exit_rollback;
    }

    if(!parameter_create(processes, "Connection", parameter_type_string, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Connection'");
        processes = NULL;
        goto exit_rollback;
    }

exit_rollback:
    if(!processes) {
        object_rollback(root);
    }
exit:
    return processes;
}

object_t* pcb_sysbus_createTracingObject(object_t* root, object_t* processes) {
    parameter_t* param = NULL;
    // Create Traces object
    object_t* tracing = object_create(processes, "Tracing", object_attr_persistent | object_attr_accept_parameters);
    if(!tracing) {
        SAH_TRACE_ERROR("Failed to create object 'Processes'");
        goto exit;
    }

    param = parameter_create(tracing, "TraceLevel", parameter_type_uint32, parameter_attr_persistent);
    if(!param) {
        SAH_TRACE_ERROR("Failed to create parameter trace level");
        tracing = NULL;
        goto exit;
    }
    parameter_setWriteHandler(param, pcb_sysbus_tracingParameterChanged);

    param = parameter_create(tracing, "Enabled", parameter_type_bool, parameter_attr_persistent);
    if(!param) {
        SAH_TRACE_ERROR("Failed to create parameter enable");
        tracing = NULL;
        goto exit;
    }
    parameter_setWriteHandler(param, pcb_sysbus_tracingParameterChanged);
    parameter_setFromChar(param, "true");

exit:
    if(!tracing) {
        object_rollback(root);
    }
    return tracing;
}

static bool pcb_sysbus_addProcessesFunction(object_t* root, object_t* processes) {
    // add function save
    function_t* fn = function_create(processes, "save", function_type_bool, function_attr_template_only);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, pcb_sysbus_processesSave);
    argument_create(fn, "fileName", argument_type_string, 0);

    // add function load
    fn = function_create(processes, "load", function_type_bool, function_attr_template_only);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, pcb_sysbus_processesLoad);
    argument_create(fn, "fileName", argument_type_string, 0);

    // add function register
    fn = function_create(processes, "register", function_type_string, function_attr_template_only);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, pcb_sysbus_processRegister);
    argument_create(fn, "name", argument_type_string, argument_attr_default);
    argument_create(fn, "sync", argument_type_bool, argument_attr_out);

    return true;
}

bool pcb_sysbus_createProcessesDefinition(pcb_t* pcb) {
    if(!pcb) {
        return false;
    }

    // fetch the root of the datamodel
    object_t* root = datamodel_root(pcb_datamodel(pcb));

    // add definition of processes object
    object_t* processes = pcb_sysbus_createProcessesObject(root);
    if(!processes) {
        return false;
    }
    object_aclSet(processes, UINT32_MAX, 0);

    // add definition of tracing object
    object_t* tracing = pcb_sysbus_createTracingObject(root, processes);
    if(!tracing) {
        return false;
    }

    return pcb_sysbus_addProcessesFunction(root, processes);
}

object_t* pcb_sysbus_getInterconnection(peer_info_t* peer, object_t* processes) {
    system_object_info_t* info = NULL;
    object_t* instance = NULL;
    object_for_each_instance(instance, processes) {
        info = (system_object_info_t*) object_getUserData(instance);
        if(!info) {
            continue;
        }
        if(peer == info->peer) {
            const char* type = object_da_parameterCharValue(instance, "Type");
            if(strcmp(type, "Bus") == 0) {
                return instance;
            }
            break;
        }
    }

    return NULL;
}

object_t* pcb_sysbus_addProcess(pcb_t* pcb, peer_info_t* peer, notification_t* notification) {
    SAH_TRACE_IN();
    if(!pcb || !notification) {
        SAH_TRACE_OUT();
        return NULL;
    }

    object_t* processes = pcb_getObject(pcb, "Process", path_attr_default);
    if(!processes) {
        SAH_TRACE_OUT();
        return NULL;
    }

    // verify all current processes, remove or update the "dead" ones
    pcb_sysbus_verifyProcesses(processes);

    // Check for interconnection, if interconnected the other bus is handling the details
    object_t* interconnected = pcb_sysbus_getInterconnection(peer, processes);
    // Get the application name
    notification_parameter_t* param_name = notification_getParameter(notification, "Application");

    if(!param_name) {
        SAH_TRACE_ERROR("No name notification");
        SAH_TRACE_OUT();
        return NULL;
    }

    // create system information
    SAH_TRACE_INFO("Search or create process ...");
    object_t* instance = pcb_sysbus_createProcess(pcb, peer, notification, interconnected, NULL);
    system_object_info_t* info = object_getUserData(instance);

    if(!instance || !info) {
        SAH_TRACE_ERROR("Failed to create process");
        SAH_TRACE_OUT();
        return NULL;
    }
    SAH_TRACE_INFO("Process available %s", object_name(instance, path_attr_key_notation));

    // set process data from notification
    SAH_TRACE_INFO("Take trace info from application ? %s", (object_state(instance) == object_state_created)? "Yes":"No");
    pcb_sysbus_setProcessData(instance, peer, notification, (object_state(instance) == object_state_created));

    SAH_TRACE_OUT();
    return instance;
}

void pcb_sysbus_processRemoveMtk(object_t* process) {
    // remove functions
    function_t* fn = object_getFunction(process, "getEnv");
    function_delete(fn);
    fn = object_getFunction(process, "setEnv");
    function_delete(fn);
    fn = object_getFunction(process, "registerSharedObject");
    function_delete(fn);
    fn = object_getFunction(process, "unregisterSharedObject");
    function_delete(fn);
    fn = object_getFunction(process, "rpcFinish");
    function_delete(fn);
    fn = object_getFunction(process, "unregister");
    function_delete(fn);
    fn = object_getFunction(process, "loadSharedObject");
    function_delete(fn);
    fn = object_getFunction(process, "unloadSharedObject");
    function_delete(fn);

    // remove parameter
    parameter_t* param = object_getParameter(process, "EnableSync");
    if(parameter_state(param) == parameter_state_created) {
        parameter_rollback(param);
    } else {
        parameter_delete(param);
    }

    // remove sub-objects
    object_t* sharedObject = object_getObjectByKey(process, "SharedObject");
    if(object_state(sharedObject) == object_state_created) {
        object_rollback(sharedObject);
    } else {
        object_delete(sharedObject);
    }
}

void pcb_sysbus_processReset(object_t* process, const char* status) {
    SAH_TRACE_INFO("Resetting process (%s) information, using state %s", object_name(process, path_attr_key_notation), status);

    object_parameterSetCharValue(process, "Name", "");
    object_parameterSetUInt32Value(process, "PID", 0);
    object_parameterSetCharValue(process, "NameSpace", "");
    object_parameterSetCharValue(process, "Status", status);
    object_parameterSetCharValue(process, "Type", "");
    object_parameterSetCharValue(process, "Connection", "");

    // reset all registered parameters to false
    object_parameterSetBoolValue(process, "Registered", false);
    object_t* sharedObjects = object_getObjectByKey(process, "SharedObject");
    object_t* sharedObject = NULL;
    object_for_each_instance(sharedObject, sharedObjects) {
        mtk_process_unregisterSharedObject(sharedObject);
        object_parameterSetCharValue(sharedObject, "Path", "");
    }

    object_commit(process);
}

void pcb_sysbus_enableTraceZones(object_t* tracing) {
    parameter_t* parameter = NULL;

    object_for_each_parameter(parameter, tracing) {
        if(strcmp(parameter_name(parameter), "Enabled") == 0) {
            continue;
        }
        uint32_t value = object_parameterUInt32Value(tracing, parameter_name(parameter));
        if(strcmp(parameter_name(parameter), "TraceLevel") == 0) {
            sahTraceSetLevel(value);
            continue;
        }
        parameter_setWriteHandler(parameter, pcb_sysbus_tracingParameterChanged);
        sah_trace_zone_it* zone = sahTraceGetZone(parameter_name(parameter));
        if(zone) {
            sahTraceZoneSetLevel(zone, value);
        } else {
            sahTraceAddZone(value, parameter_name(parameter));
        }
    }

    object_commit(tracing);
}

void pcb_sysbus_deleteProcess(pcb_t* pcb, peer_info_t* peer, notification_t* notification) {
    if(!pcb) {
        return;
    }

    object_t* processes = pcb_getObject(pcb, "Process", path_attr_default);
    if(!processes) {
        return;
    }

    notification_parameter_t* param_name = notification_getParameter(notification, "Application");
    notification_parameter_t* param_bus_name = notification_getParameter(notification, "BusName");

    notification_parameter_t* param_object = notification_getParameter(notification, "Object");
    while(param_object) {
        notification_parameter_destroy(param_object);
        param_object = notification_getParameter(notification, "Object");
    }

    object_t* interconnected = pcb_sysbus_getInterconnection(peer, processes);
    char* name = notification_parameter_value(param_name);
    char* bus_name = notification_parameter_value(param_bus_name);
    object_t* object = NULL;

    if(bus_name) {
        SAH_TRACE_INFO("Searching process [%s]", bus_name);
        object = object_getObject(processes, bus_name, path_attr_key_notation, NULL);
    } else {
        if(interconnected) {
            SAH_TRACE_INFO("Searching process [%s] (interconnected)", name);
            object = object_getObject(processes, name, path_attr_key_notation, NULL);
        } else {
            SAH_TRACE_INFO("Searching process [%s_%s] (local)", appconfig.name, name);
            object = object_getObjectByKey(processes, "%s_%s", appconfig.name, name);
        }
        if(object) {
            notification_parameter_t* notif_param = notification_parameter_create("BusName", object_name(object, path_attr_key_notation));
            notification_addParameter(notification, notif_param);
        }
    }
    free(name);
    free(bus_name);

    if(!object) {
        // no process found, nothing to do
        return;
    }

    object_t* root = datamodel_root(pcb_datamodel(appconfig.pcb));

    // check if the found process has child processes, remove them as well.
    SAH_TRACE_INFO("Removing child processes");
    object_t* proc = NULL;
    object_for_each_instance(proc, processes) {
        system_object_info_t* proc_info = object_getUserData(proc);
        if(!proc_info) {
            SAH_TRACE_NOTICE("No info object added to process, skip");
            continue;
        }
        if(proc_info->process == object) {
            object_delete(proc);
        }
    }

    object_t* dispatchobject = NULL;
    object_for_each_child(dispatchobject, root) {
        SAH_TRACE_INFO("Dispatch object name = %s", object_name(dispatchobject, path_attr_key_notation));
        system_object_info_t* proc_info = object_getUserData(dispatchobject);
        if(!proc_info ||
           (proc_info->type == system_object_internal) ||
           (proc_info->type == system_object_self)) {
            continue;
        }

        if(proc_info->process == object) {
            SAH_TRACE_INFO("Delete dispatch object name = %s", object_name(dispatchobject, path_attr_key_notation));
            notification_parameter_t* notif_param_proc = notification_parameter_create("Object", object_name(dispatchobject, path_attr_key_notation));
            notification_addParameter(notification, notif_param_proc);
            proc_info->peer = NULL;
            proc_info->process = NULL;

            object_delete(dispatchobject);
        }
    }

    // local process, reset parameters
    pcb_sysbus_processReset(object, "Stopped");

    object_commit(root);
}

void pcb_sysbus_tracingParameterChanged(parameter_t* parameter, const variant_t* oldvalue) {
    SAH_TRACE_IN();
    (void) oldvalue;
    object_t* object = parameter_owner(parameter);
    if(!object) {
        SAH_TRACE_ERROR("Parameter is not owned by an object");
        SAH_TRACE_OUT();
        return;
    }

    if(object_state(object) == object_state_created) {
        SAH_TRACE_INFO("Process (%s) object was just created, ignore ", object_name(object_parent(object), path_attr_key_notation));
        SAH_TRACE_OUT();
        return;
    }

    // process object is parent of tracing object
    object = object_parent(object);

    SAH_TRACE_INFO("Trace parameter has changed, notify application %s", object_name(object, path_attr_key_notation));
    system_object_info_t* info = (system_object_info_t*) object_getUserData(object);
    if(!info) {
        SAH_TRACE_ERROR("No information available");
        SAH_TRACE_OUT();
        return;
    }

    if(info->type == system_object_self) {
        SAH_TRACE_INFO("Change trace levels of bus");
        pcb_sysbus_changeTraceZones(parameter);
        SAH_TRACE_OUT();
        return;
    }

    if(!info->peer) {
        SAH_TRACE_ERROR("No peer information available");
        SAH_TRACE_OUT();
        return;
    }

    if(!peer_isConnected(info->peer)) {
        SAH_TRACE_ERROR("Peer is not connected");
        SAH_TRACE_OUT();
        return;
    }

    const variant_t* param_value = parameter_getValue(parameter);
    string_t value;
    string_initialize(&value, 64);
    variant_toString(&value, param_value);
    notification_t* notification = notification_createApplicationChangeSetting("Group", "Tracing");
    notification_parameter_t* notif_param = notification_parameter_create(parameter_name(parameter), string_buffer(&value));
    notification_addParameter(notification, notif_param);

    // when tracing is switched from disabled to enabled, send all trace zones and tracelevel
    if((strcmp(parameter_name(parameter), "Enabled") == 0) && variant_bool(param_value)) {
        object_t* tracing = parameter_owner(parameter);
        object_for_each_parameter(parameter, tracing) {
            if(strcmp(parameter_name(parameter), "Enabled") == 0) {
                continue;
            }
            param_value = parameter_getValue(parameter);
            variant_toString(&value, param_value);
            notif_param = notification_parameter_create(parameter_name(parameter), string_buffer(&value));
            notification_addParameter(notification, notif_param);
        }
    }
    string_cleanup(&value);

    if(notification) {
        pcb_replyBegin(info->peer, 0, 0);
        pcb_writeNotification(info->peer, 0, notification);
        pcb_replyEnd(info->peer, 0);
        notification_destroy(notification);
    }
    SAH_TRACE_OUT();
}

void pcb_sysbus_processSyncParameterChanged(parameter_t* parameter, const variant_t* oldvalue) {
    SAH_TRACE_IN();
    (void) oldvalue;
    object_t* process = parameter_owner(parameter);
    system_object_info_t* info = object_getUserData(process);

    if(!info) {
        SAH_TRACE_NOTICE("No object information available - internal object, skip");
        SAH_TRACE_OUT();
        return;
    }

    if(info->type == system_object_self) {
        SAH_TRACE_INFO("EnableSync set on the bus itself, ignore");
        SAH_TRACE_OUT();
        return;
    }

    if(object_state(process) == object_state_created) {
        SAH_TRACE_INFO("Process (%s) object was just created, ignore", object_name(process, path_attr_key_notation));
        SAH_TRACE_OUT();
        return;
    }

    if(!info->peer) {
        SAH_TRACE_ERROR("No peer information available");
        SAH_TRACE_OUT();
        return;
    }

    if(!peer_isConnected(info->peer)) {
        SAH_TRACE_ERROR("Peer is not connected");
        SAH_TRACE_OUT();
        return;
    }
    SAH_TRACE_INFO("EnableSync parameter has changed, notify application %s", object_name(process, path_attr_key_notation));

    const variant_t* param_value = parameter_getValue(parameter);
    char* value = variant_char(param_value);
    notification_t* notification = notification_createApplicationChangeSetting("EnableSync", value);
    free(value);

    if(notification) {
        pcb_replyBegin(info->peer, 0, 0);
        pcb_writeNotification(info->peer, 0, notification);
        pcb_replyEnd(info->peer, 0);
        notification_destroy(notification);
    }
    SAH_TRACE_OUT();
}

function_exec_state_t pcb_sysbus_processRegister(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    function_exec_state_t fstate = function_exec_error;
    request_t* request = fcall_request(fcall);
    peer_info_t* peer = fcall_peer(fcall);
    object_t* processes = fcall_object(fcall);
    object_t* root = datamodel_root(pcb_datamodel(appconfig.pcb));

    uint32_t pid = request_getPid(request);
    const char* type = "Client";

    char* name = NULL;
    bool sync = false;
    variant_list_t* objects = NULL;
    variant_list_t* objects_attr = NULL;
    uint32_t tracelevel = 0;
    variant_map_t* tracezones = NULL;
    char* ns = NULL;

    object_t* tracing = NULL;
    parameter_t* param = NULL;
    notification_t* notify = NULL;

    // fetch arguments
    argument_getChar(&name, args, request_attributes(request), "name", NULL);
    argument_getBool(&sync, args, request_attributes(request), "sync", false);
    argument_getList(&objects, args, request_attributes(request), "objects", NULL);
    argument_getList(&objects_attr, args, request_attributes(request), "objectsattr", NULL);
    argument_getUInt32(&tracelevel, args, request_attributes(request), "tracelevel", 0);
    argument_getMap(&tracezones, args, request_attributes(request), "tracezones", NULL);
    argument_getChar(&ns, args, request_attributes(request), "namespace", NULL);

    SAH_TRACE_INFO("Search or create process ...");
    object_t* instance = pcb_sysbus_createProcess(appconfig.pcb, peer, NULL, NULL, name);
    system_object_info_t* info = object_getUserData(instance);

    if(!instance || !info) {
        SAH_TRACE_ERROR("Failed to create process");
        reply_addError(request_reply(request), pcb_error_invalid_name, "Process registration failed", name);
        goto leave;
    }

    variant_setChar(retval, object_name(instance, path_attr_key_notation));
    if(!sync) {
        sync = object_parameterBoolValue(instance, "EnableSync");
    }

    // set process data
    if((objects && !variant_list_isEmpty(objects)) || (ns && *ns)) {
        type = "Plug-in";
    }
    object_parameterSetBoolValue(instance, "Registered", true);
    object_parameterSetCharValue(instance, "Status", "Ready");
    object_parameterSetCharValue(instance, "Type", type);
    object_parameterSetUInt32Value(instance, "PID", pid);
    object_parameterSetCharValue(instance, "Name", name);
    object_parameterSetCharValue(instance, "Namespace", ns);
    if(peer_isIPCSocket(peer)) {
        object_parameterSetCharValue(instance, "Connection", "IPC");
    } else {
        object_parameterSetCharValue(instance, "Connection", "TCP");
    }

    // add dispatching objects
    if(ns && *ns) {
        // if a namespace is given use that one
        SAH_TRACE_INFO("Add Namespace dispatch object = %s", ns);
        if(!pcb_sysbus_dispatchObjectCreate(appconfig.pcb, peer, ns, object_attr_default, system_object_dispatch_namespace, instance)) {
            SAH_TRACE_ERROR("Failed to add namespace [%s]", ns);
        }
    } else {
        // otherwise use the list of objects
        char** object = NULL;
        uint32_t* object_attr = NULL;
        for(object = variant_list_firstChar(objects), object_attr = variant_list_firstUInt32(objects_attr);
            object;
            object = variant_list_iterator_nextChar(object), object_attr = variant_list_iterator_nextUInt32(object_attr)) {
            SAH_TRACE_INFO("Add dispatch object = %s object_attr = %d", *object, *object_attr);
            if(!pcb_sysbus_dispatchObjectCreate(appconfig.pcb, peer, *object, *object_attr, system_object_dispatch, instance)) {
                SAH_TRACE_ERROR("Failed to add dispatch object [%s] object_attr [%d]", *object, *object_attr);
            }
        }
    }

    // set trace zones
    tracing = object_getObjectByKey(instance, "Tracing");
    param = NULL;
    if(object_state(instance) == object_state_created) {
        SAH_TRACE_INFO("Apply tracing info from app");
        object_parameterSetUInt32Value(tracing, "TraceLevel", tracelevel);
        variant_map_iterator_t* it = NULL;
        variant_map_for_each(it, tracezones) {
            SAH_TRACE_INFO("Add trace zone %s = %d", variant_map_iterator_key(it), variant_uint32(variant_map_iterator_data(it)));
            param = parameter_create(tracing, variant_map_iterator_key(it), parameter_type_int32, parameter_attr_persistent);
            char* value = variant_char(variant_map_iterator_data(it));
            parameter_setFromChar(param, value);
            parameter_setWriteHandler(param, pcb_sysbus_tracingParameterChanged);
            free(value);
            value = NULL;
        }
    } else {
        object_for_each_parameter(param, tracing) {
            parameter_setWriteHandler(param, pcb_sysbus_tracingParameterChanged);
        }
        // add all available trace zones and settings in the data model to the reply
        SAH_TRACE_INFO("Send tracing info to appplication");
        variant_t traces;
        variant_initialize(&traces, variant_type_map);
        variant_map_t* trace_map = variant_da_map(&traces);
        variant_map_addBool(trace_map, "Enabled", object_parameterBoolValue(tracing, "Enabled"));
        variant_map_addUInt32(trace_map, "TraceLevel", object_parameterUInt32Value(tracing, "TraceLevel"));
        parameter_t* trace_zone = NULL;
        object_for_each_parameter(trace_zone, tracing) {
            if(strcmp(parameter_name(trace_zone), "Enabled") == 0) {
                continue;
            }
            variant_map_addUInt32(trace_map, parameter_name(trace_zone), object_parameterUInt32Value(tracing, parameter_name(trace_zone)));
        }
        argument_valueAdd(args, "traces", &traces);
        variant_cleanup(&traces);
    }

    // set the return value
    if(sync) {
        variant_t variant;
        variant_initialize(&variant, variant_type_bool);
        variant_setBool(&variant, sync);
        argument_valueAdd(args, "sync", &variant);
        variant_cleanup(&variant);
    }

    SAH_TRACE_INFO("Forward stored notification requests");
    // this must be done before commiting
    pcb_sysbus_forward_notifyRequests(root, peer);

    SAH_TRACE_INFO("Process %s added, type = %s", object_name(instance, path_attr_key_notation), type);
    object_commit(root);

    // create application started notification
    notify = pcb_sysbus_createApplicationStarted(root, instance);
    SAH_TRACE_INFO("Sending started notification for process: %s", object_name(instance, path_attr_key_notation));
    pcb_sendNotification(appconfig.pcb, NULL, peer, notify);
    notification_destroy(notify);

    fstate = function_exec_done;
leave:
    if(fstate == function_exec_error) {
        object_rollback(processes);
    }
    free(ns);
    variant_list_cleanup(objects);
    variant_list_cleanup(objects_attr);
    variant_map_cleanup(tracezones);
    free(tracezones);
    free(objects);
    free(objects_attr);
    free(name);
    fcall_destroy(fcall);
    return fstate;
}


function_exec_state_t pcb_sysbus_processUnregister(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    pcb_t* pcb = datamodel_pcb(object_datamodel(fcall_object(fcall)));
    object_t* process = fcall_object(fcall);
    object_t* root = object_root(process);
    object_t* processes = NULL;

    if(object_parent(process) == root) {
        char* name = NULL;
        argument_getChar(&name, args, request_attributes(fcall_request(fcall)), "process", NULL);
        processes = object_getObjectByKey(root, "Process");
        process = object_getObjectByKey(processes, name);
        free(name);
    } else {
        processes = object_parent(process);
    }
    system_object_info_t* info = NULL;
    info = (system_object_info_t*) object_getUserData(process);

    if(!info || !info->peer) {
        SAH_TRACE_WARNING("Trying to disconnect internal object or self");
        goto exit_error;
    }

    if(info->process) {
        // Create application stopping notification and send it
        notification_t* stop = pcb_sysbus_createApplicationStopping(root, process);
        SAH_TRACE_INFO("Sending stopping notification for process: %s", object_name(process, path_attr_key_notation));
        pcb_sendNotification(pcb, NULL, info->peer, stop);
        notification_destroy(stop);
        // it has a parent process, only remove process object and dispatch objects
        SAH_TRACE_INFO("Processes has a parent process, removing objects");
        object_delete(process);
        object_commit(processes);
    } else {
        object_parameterSetCharValue(process, "Status", "Stopping");
        // no parent process, close and destroy the peer
        SAH_TRACE_INFO("Closing peer");
        pcb_timer_defer(defered_peer_destroy, info, 0);
    }

    variant_setBool(retval, true);
    SAH_TRACE_OUT();
    return function_exec_done;

exit_error:
    variant_setBool(retval, false);
    SAH_TRACE_OUT();
    return function_exec_error;
}
