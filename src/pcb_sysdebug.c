/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <components.h>

#include "pcb_sysdatamodel.h"
#include "parse_args.h"

#include <pcb/core/types.h>
#include <pcb/core/notification.h>
#include "pcb_sysdispatching.h"
#include "pcb_sysdatamodel.h"
#include "pcb_sysdebug_verify.h"
#include "pcb_sysutils.h"

extern application_config_t appconfig;

static variant_list_iterator_t* sysbus_createDispatchInfo(object_t* dispatch) {
    variant_map_t* object_data = NULL;

    variant_list_iterator_t* it = variant_list_iterator_createMapMove(NULL);
    object_data = variant_da_map(variant_list_iterator_data(it));
    variant_map_addChar(object_data, "Object", object_name(dispatch, path_attr_key_notation));
    system_object_info_t* info = object_getUserData(dispatch);
    if(info && info->process && (info->type != system_object_internal)) {
        system_object_info_t* route_info = object_getUserData(info->process);
        variant_map_addChar(object_data, "Destination", object_name(info->process, path_attr_key_notation));
        if(route_info && route_info->process) {
            variant_map_addChar(object_data, "Route", object_name(route_info->process, path_attr_key_notation));
        }
        variant_map_addChar(object_data, "Type", (info->type == system_object_dispatch) ? "Dispatch" : "Namespace");
    } else {
        variant_map_addChar(object_data, "Destination", "");
        variant_map_addChar(object_data, "Type", "Internal");
    }

    return it;
}

static function_exec_state_t sysbus_listDispatchObjects(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    object_t* root = object_root(fcall_object(fcall));
    object_t* processes = object_getObject(root, "Process", path_attr_key_notation, NULL);
    uint32_t attr = request_attributes(fcall_request(fcall));

    char* name = NULL;

    argument_getChar(&name, args, attr, "name", NULL);

    variant_initialize(retval, variant_type_array);
    variant_list_t* objects = variant_da_list(retval);
    object_t* dispatch = NULL;
    object_t* process = NULL;

    // if no name is given add all dispatch objects
    if(!name) {
        object_for_each_child(dispatch, root) {
            variant_list_iterator_t* it = sysbus_createDispatchInfo(dispatch);
            variant_list_append(objects, it);
        }
        goto leave;
    }

    process = object_getObjectByKey(processes, name);
    // if the name given is a process, add all dispatch objects for that process
    if(process) {
        system_object_info_t* proc_info = NULL;
        object_for_each_child(dispatch, root) {
            proc_info = object_getUserData(dispatch);
            if(!proc_info || (proc_info->process != process)) {
                continue;
            }
            variant_list_iterator_t* it = sysbus_createDispatchInfo(dispatch);
            variant_list_append(objects, it);
        }
        goto leave;
    }

    // if the name given is an object, only add that object
    dispatch = object_getObject(root, name, path_attr_key_notation, NULL);
    if(dispatch) {
        variant_list_iterator_t* it = sysbus_createDispatchInfo(dispatch);
        variant_list_append(objects, it);
    }

leave:
    free(name);
    fcall_destroy(fcall);
    return function_exec_done;
}

static function_exec_state_t sysbus_removeDispatchObject(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    uint32_t attr = request_attributes(fcall_request(fcall));

    object_t* root = object_root(fcall_object(fcall));
    char* object = NULL;

    // take arguments
    argument_getChar(&object, args, attr, "object", NULL);

    object_t* remove_object = object_getObject(root, object, path_attr_key_notation, NULL);
    if(!remove_object) {
        reply_addError(request_reply(fcall_request(fcall)), pcb_error_not_found, error_string(pcb_error_not_found), object);
        goto exit_error;
    }

    object_delete(remove_object);
    object_commit(root);

    variant_setBool(retval, true);

    free(object);
    fcall_destroy(fcall);
    return function_exec_done;

exit_error:
    free(object);
    variant_setBool(retval, false);
    fcall_destroy(fcall);
    return function_exec_error;
}

static function_exec_state_t sysbus_addTraceZone(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();
    uint32_t attr = request_attributes(fcall_request(fcall));

    object_t* process_object = fcall_object(fcall);
    object_t* processes_object = object_parent(process_object);
    char* process = NULL;
    char* zone = NULL;
    uint32_t level = 0;

    object_t* traces_object = NULL;
    parameter_t* parameter = NULL;
    system_object_info_t* info = NULL;

    // take arguments
    if(strcmp(object_name(process_object, path_attr_key_notation), "Debug") == 0) {
        // Debug object, take process name from argument list and search for object
        object_t* root = object_parent(process_object);
        processes_object = object_getObject(root, "Process", 0, NULL);
        argument_getChar(&process, args, attr, "process", NULL);
        process_object = object_getObject(processes_object, process, path_attr_key_notation, NULL);
        if(!process_object) {
            reply_addError(request_reply(fcall_request(fcall)), pcb_error_not_found, error_string(pcb_error_not_found), process);
            goto exit_error;
        }
    }
    argument_getChar(&zone, args, attr, "zone", NULL);
    argument_getUInt32(&level, args, attr, "level", 0);

    info = (system_object_info_t*) object_getUserData(process_object);
    if(!info) {
        SAH_TRACE_ERROR("No information available");
        goto exit_error;
    }

    traces_object = object_getObject(process_object, "Tracing", path_attr_default, NULL);
    parameter = object_getParameter(traces_object, zone);
    if(!parameter) {
        parameter = parameter_create(traces_object, zone, parameter_type_string, parameter_attr_persistent);
        if(!parameter) {
            reply_addError(request_reply(fcall_request(fcall)), pcb_error, error_string(pcb_error), zone);
            goto exit_error;
        }
        if(info->type == system_object_self) {
            SAH_TRACE_INFO("Change trace levels of sysbus");
            parameter_setWriteHandler(parameter, pcb_sysbus_tracingParameterChanged);
        } else {
            parameter_setWriteHandler(parameter, pcb_sysbus_tracingParameterChanged);
        }
    }
    object_commit(processes_object);

    object_parameterSetUInt32Value(traces_object, zone, level);
    object_commit(traces_object);

    free(process);
    free(zone);
    variant_setBool(retval, true);
    fcall_destroy(fcall);
    SAH_TRACE_OUT();
    return function_exec_done;

exit_error:
    free(process);
    free(zone);
    variant_setBool(retval, false);
    fcall_destroy(fcall);
    SAH_TRACE_OUT();
    return function_exec_error;
}

static function_exec_state_t sysbus_verifyPlugins(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    uint32_t attr = request_attributes(fcall_request(fcall));

    object_t* root = object_parent(fcall_object(fcall));
    object_t* processes_object = object_getObject(root, "Process", 0, NULL);

    verify_data_t* data = (verify_data_t*) calloc(1, sizeof(verify_data_t));
    if(!data) {
        return function_exec_error;
    }

    // take arguments
    argument_getUInt32(&data->depth, args, attr, "depth", 0);
    argument_getUInt32(&data->timeout, args, attr, "timeout", 5000);

    // initialize return value to an empty list
    variant_initialize(&data->retval, variant_type_map);

    data->timer = pcb_timer_create();
    pcb_timer_setHandler(data->timer, sysbus_verifyTimeout); // stop the request, take next process and send request
    pcb_timer_setUserData(data->timer, data);

    // set the fcall pointer
    data->fcall = fcall;

    // take first process and fetch objects with depth and start timer with timeout
    object_t* proc = object_firstInstance(processes_object);

    // start verification
    if(!sysbus_verify(proc, data)) {
        variant_copy(retval, &data->retval);
        variant_cleanup(&data->retval);
        pcb_timer_destroy(data->timer);
        free(data);
        return function_exec_done;
    }

    // set cancel handler
    fcall_setCancelHandler(fcall, sysbus_verifyCanceled);
    fcall_setUserData(fcall, data);

    // return executing, keep on running until all processes are done
    return function_exec_executing;
}

static function_exec_state_t sysbus_resetTracing(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    uint32_t attr = request_attributes(fcall_request(fcall));

    char* output = NULL;

    // take arguments
    argument_getChar(&output, args, attr, "output", "syslog");

    if(strcmp(output, "syslog") == 0) {
        appconfig.trace_output = TRACE_TYPE_SYSLOG;
    } else if(strcmp(output, "stdout") == 0) {
        appconfig.trace_output = TRACE_TYPE_STDOUT;
    }

    // close tracing
    sahTraceClose();

    // initialize tracing and logging
    sahTraceOpen(appconfig.name, appconfig.trace_output);
    sahTraceSetLevel(appconfig.trace_level);

    variant_setBool(retval, true);
    free(output);
    return function_exec_done;
}

static function_exec_state_t sysbus_listRequests(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    uint32_t attr = request_attributes(fcall_request(fcall));
    char* requests = NULL;

    // take arguments
    argument_getChar(&requests, args, attr, "requests", "notify");

    request_list_t list;
    request_list_initialize(&list);

    connection_info_t* con = peer_connection(fcall_peer(fcall));
    connection_t* c = NULL;
    llist_iterator_t* it = NULL;
    SAH_TRACE_INFO("Building request list ...");
    llist_for_each(it, &con->connections) {
        c = llist_item_data(it, connection_t, it);
        if(c->info.socketfd == -1) {
            continue;
        }
        if(strcmp(requests, "notify") == 0) {
            SAH_TRACE_INFO("Adding notify request for fd = %d", c->info.socketfd);
            pcb_getNotifyRequests(&c->info, &list);
        }
        if(strcmp(requests, "forwarded") == 0) {
            SAH_TRACE_INFO("Adding forwarded request for fd = %d", c->info.socketfd);
            pcb_getForwardedRequests(&c->info, &list);
        }
    }

    variant_list_t data;
    variant_list_iterator_t* mapit = NULL;
    variant_map_t* map = NULL;
    variant_list_initialize(&data);
    request_t* req = request_list_first(&list);
    SAH_TRACE_INFO("Building return value ...");
    while(req) {
        // Address added for debugging purposes! Reference not to be used elsewhere
        char address[16];
        snprintf(address, 15, "%p", req);
        mapit = variant_list_iterator_createMapMove(NULL);
        map = variant_da_map(variant_list_iterator_data(mapit));
        if(map) {
            SAH_TRACE_INFO("Adding request for path [%s]", request_path(req));
            variant_map_addChar(map, "path", request_path(req));
            variant_map_addUInt32(map, "type", request_type(req));
            variant_map_addUInt32(map, "depth", request_depth(req));
            variant_map_addUInt32(map, "attributes", request_attributes(req));
            variant_map_addUInt32(map, "UID", request_userID(req));
            variant_map_addUInt32(map, "SourcePID", request_getPid(req));
            variant_map_addChar(map, "Address", address);
            variant_list_append(&data, mapit);
        } else {
            variant_list_iterator_destroy(mapit);
        }
        req = request_list_next(req);
    }
    llist_cleanup(&list);
    variant_setListMove(retval, &data);
    variant_list_clear(&data);
    free(requests);

    SAH_TRACE_OUT();
    return function_exec_done;
}

static function_exec_state_t sysbus_sniff(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();

    uint32_t attr = request_attributes(fcall_request(fcall));
    uint32_t pid = 0;
    bool enable = true;

    // take arguments
    argument_getUInt32(&pid, args, attr, "pid", 0);
    argument_getBool(&enable, args, attr, "enable", true);

    if(enable) {
        uint32_t* active_pid = NULL;
        variant_list_for_each_uint32(active_pid, &appconfig.sniffers) {
            if(*active_pid == pid) {
                goto exit;
            }
        }
        variant_list_addUInt32(&appconfig.sniffers, pid);
    } else {
        variant_list_iterator_t* it = NULL;
        variant_list_for_each(it, &appconfig.sniffers) {
            if(variant_uint32(variant_list_iterator_data(it)) == pid) {
                break;
            }
        }
        if(it) {
            variant_list_iterator_destroy(it);
        }
    }

    variant_initialize(retval, variant_type_unknown);

exit:
    SAH_TRACE_OUT();
    return function_exec_done;
}

static function_exec_state_t sysbus_getObjectPid(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {

    char* name = NULL;
    object_t* obj = NULL;
    pid_t pid = 0;
    function_exec_state_t exec_state = function_exec_error;

    object_t* root = object_root(fcall_object(fcall));
    object_t* processes = object_getObject(root, "Process", path_attr_key_notation, NULL);
    uint32_t attr = request_attributes(fcall_request(fcall));

    variant_initialize(retval, variant_type_uint32);
    argument_getChar(&name, args, attr, "name", NULL);

    if(!name || !(*name)) {
        fcall_addErrorReply(fcall, pcb_error_function_argument_missing, "Argument is missing", "object name");
        goto exit_nofree;
    }

    obj = object_getObject(root, name, path_attr_key_notation, NULL);
    if(obj) {
        system_object_info_t* info = object_getUserData(obj);
        const char* process_name = object_name(info->process, path_attr_key_notation);
        object_t* process = object_getObjectByKey(processes, process_name);
        if(process) {
            pid = object_parameterInt32Value(process, "PID");
        } else {
            fcall_addErrorReply(fcall, pcb_error_not_found, error_string(pcb_error_not_found), process_name);
            goto exit;
        }
    } else {
        fcall_addErrorReply(fcall, pcb_error_not_found, error_string(pcb_error_not_found), name);
        goto exit;
    }

    variant_setUInt32(retval, pid);
    exec_state = function_exec_done;

exit:
    free(name);
exit_nofree:
    fcall_destroy(fcall);
    return exec_state;
}

bool pcb_sysbus_createDebugDefinition(pcb_t* pcb) {
    object_t* root = datamodel_root(pcb_datamodel(pcb));

    // fetch processes object
    object_t* processes = object_getObjectByKey(root, "Process");

    // create Debug object
    object_t* dbg = object_create(root, "Debug", 0);
    if(!dbg) {
        object_rollback(root);
        return false;
    }
    object_aclSet(dbg, UINT32_MAX, 0);

    // add function listdispatchTable
    function_t* fn = function_create(dbg, "listDispatchTable", function_type_list, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_listDispatchObjects);
    argument_create(fn, "name", argument_type_string, 0);

    // add function removeDispatchObject
    fn = function_create(dbg, "removeDispatchObject", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_removeDispatchObject);
    argument_create(fn, "object", argument_type_string, argument_attr_mandatory);

    // add function addTraceZone in debug object
    fn = function_create(dbg, "addTraceZone", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_addTraceZone);
    argument_create(fn, "process", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "zone", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "level", argument_type_uint32, 0);

    // add function addTraceZone in debug object
    fn = function_create(processes, "addTraceZone", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_addTraceZone);
    argument_create(fn, "zone", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "level", argument_type_uint32, 0);

    // add function verifyPlugins
    fn = function_create(dbg, "verifyPlugins", function_type_list, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_verifyPlugins);
    argument_create(fn, "depth", argument_type_uint32, 0);
    argument_create(fn, "timeout", argument_type_uint32, 0);

    // add function busResetTrace
    fn = function_create(dbg, "busResetTrace", function_type_bool, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_resetTracing);
    argument_create(fn, "output", argument_type_string, 0);

    // add function listRequests
    fn = function_create(dbg, "listRequests", function_type_list, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_listRequests);
    argument_create(fn, "requests", argument_type_string, 0);

    // add function sniffer
    fn = function_create(dbg, "sniff", function_type_list, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_sniff);
    argument_create(fn, "pid", argument_type_uint32, 0);

    // add function getObjectPid
    fn = function_create(dbg, "getObjectPid", function_type_uint32, 0);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, sysbus_getObjectPid);
    argument_create(fn, "name", argument_type_string, 0);

    return true;
}
