/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <errno.h>
#include <stdlib.h>
#include <fnmatch.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <pcb/common.h>
#include <pcb/core.h>

#include "pcb_sysrequesthandlers.h"
#include "pcb_sysdatamodel.h"
#include "pcb_sysutils.h"

extern object_t* self;

static bool pcb_sysbus_handle_internal_object(peer_info_t* peer, request_t* req, request_source_info_t* req_info, object_t* object) {
    bool retval = true;
    uint32_t depth = 0;
    uint32_t attributes = 0;
    if(!object_canRead(object, request_userID(req))) {
        goto exit;
    }

    depth = request_depth(req);
    attributes = request_attributes(req);

    SAH_TRACE_INFO("replying internal object");
    if(req_info->first_inList) {
        req_info->first_inList = false;
    } else {
        pcb_writeObjectListNext(peer, req);
    }
    if(depth > 0) {
        retval = default_reply_object(peer, object, ((depth == UINT32_MAX) ? UINT32_MAX : depth - 1), attributes, req, NULL);
    } else {
        retval = default_reply_object(peer, object, 0, attributes & ~(request_getObject_all), req, NULL);
    }

exit:
    return retval;
}

static bool pcb_sysbus_handle_dispatch_object(peer_info_t* peer, request_t* req, system_object_info_t* info, object_t* object) {
    bool retval = true;
    uint32_t depth = 0;
    uint32_t attributes = 0;
    request_t* copy_req = NULL;

    object_t* process = info->process;
    const char* type = object_da_parameterCharValue(process, "Type");
    system_object_info_t* proc_info = object_getUserData(process);
    if(proc_info->process || (strcmp(type, "Bus") == 0)) {
        // object is in another bus
        if(info->peer == peer) {
            SAH_TRACE_INFO("Request is from the same bus as the object, do not reply");
            // request is coming from the bus that registered the object in this bus, skip it
            goto exit;
        }
        if(request_attributes(req) & request_common_include_namespace) {
            goto exit;
        }
    }

    depth = request_depth(req);
    attributes = (request_attributes(req) & ~request_common_include_namespace);

    SAH_TRACE_INFO("forwarding request for object %s", object_name(object, path_attr_key_notation));
    // copy request and forward
    copy_req = request_copy(req, depth, attributes | request_no_object_caching, request_copy_default);
    request_setPath(copy_req, object_name(object, attributes & (request_common_path_key_notation | request_common_path_slash_seperator)));
    if(!proc_info->process && (info->type == system_object_dispatch_namespace)) {
        const char* path = request_path(req);
        if(request_attributes(req) & request_common_path_slash_seperator) {
            path = strchr(path, '/');
        } else {
            path = strchr(path, '.');
        }
        if(path) {
            path++;
            request_setPath(copy_req, path);
        } else {
            request_setPath(copy_req, "");
        }
        SAH_TRACE_INFO("Must reply with namespace included");
        request_setAttributes(copy_req, attributes | request_common_include_namespace);
        SAH_TRACE_INFO("Request attributes = 0x%x", request_attributes(copy_req));
    }
    if(depth != UINT32_MAX) {
        if(depth == 0) {
            attributes = request_attributes(copy_req) & ~(request_getObject_all);
            request_setAttributes(copy_req, attributes);
        } else {
            request_setDepth(copy_req, depth - 1);
        }
    }
    if(!pcb_sysbus_forwardRequest(req, peer, copy_req, info->peer)) {
        reply_addError(request_reply(req), pcb_error, error_string(pcb_error), object_name(object, path_attr_key_notation));
        SAH_TRACE_WARNING("Failed to forward request to object %s (%s)", object_name(object, path_attr_key_notation), error_string(pcb_error));
    }

exit:
    return retval;
}

static void pcb_sysbus_forwardedDestroy(request_t* req, void* userdata) {
    (void) req;
    SAH_TRACE_IN();
    free(userdata);
    SAH_TRACE_OUT();
}

static request_source_info_t* pcb_sysbus_setForwardData(request_t* req, peer_info_t* peer) {
    request_source_info_t* req_info = (request_source_info_t*) calloc(1, sizeof(request_source_info_t));
    if(!req_info) {
        SAH_TRACE_ERROR("Failed to allocate source info");
        return NULL;
    }

    req_info->reply_started = false;
    req_info->objects_inList = false;
    req_info->objects_done = false;
    req_info->source_peer = peer;
    req_info->original_req = req;
    request_setData(req, req_info);
    request_setDestroyHandler(req, pcb_sysbus_forwardedDestroy);

    return req_info;
}

static bool pcb_sysbus_forwardedReply(request_t* req, pcb_t __attribute__((__unused__))* pcb, peer_info_t* from, void* userdata) {
    (void) from;
    SAH_TRACE_IN();
    // is called whenever a reply is available from a child request
    request_source_info_t* req_info = (request_source_info_t*) userdata;
    if(!req_info) {
        SAH_TRACE_OUT();
        return true;
    }

    uint32_t attributes = request_attributes(req);

    reply_t* reply = request_reply(req);
    reply_item_t* item = reply_firstItem(reply);
    if(!reply) {
        goto exit_reply_error;
    }

    while(item) {
        switch(reply_item_type(item)) {
        case reply_type_object:
            if(!req_info->reply_started) {
                SAH_TRACE_INFO("Reply begin");
                pcb_replyBegin(req_info->source_peer, req_info->original_req, 0);
                req_info->reply_started = true;
            }
            if(attributes & request_common_include_namespace) {
                const char* dm_namespace = datamodel_namespace(pcb_datamodel(pcb));
                SAH_TRACE_INFO("Bus namespace (%s) must be included", dm_namespace);
                datamodel_t* dm = object_datamodel(reply_item_object(item));
                datamodel_setNamespace(dm, dm_namespace);
            }
            if(attributes & request_getObject_keep_hierarchy) {
                if(req_info->objects_inList) {
                    if(req_info->first_inList) {
                        req_info->first_inList = false;
                    } else {
                        pcb_writeObjectListNext(req_info->source_peer, req);
                    }
                }
                if(!default_getObject(req_info->source_peer, reply_item_object(item), UINT32_MAX, request_attributes(req), req)) {
                    goto exit_reply_error;
                }
            } else {
                SAH_TRACE_INFO("Forwarding reply object type");
                pcb_writeObjectBegin(req_info->source_peer, req, reply_item_object(item));
                if(!default_replyParameters(req_info->source_peer, reply_item_object(item), req)) {
                    goto exit_reply_error;
                }
                pcb_writeObjectEnd(req_info->source_peer, req);
            }
            break;
        case reply_type_notification:
            SAH_TRACE_INFO("Forwarding reply notification type");
            if(!req_info->reply_started) {
                SAH_TRACE_INFO("Reply begin");
                pcb_replyBegin(req_info->source_peer, req_info->original_req, 0);
                req_info->reply_started = true;
            }
            pcb_writeNotification(req_info->source_peer, req, reply_item_notification(item));
            break;
        case reply_type_error: {
            SAH_TRACE_INFO("Forwarding reply error type (error =0x%X: %s -%s)", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
        }
        break;
        case reply_type_function_return: {
            SAH_TRACE_INFO("Forwarding reply function return type");
            if(!req_info->reply_started) {
                SAH_TRACE_INFO("Reply begin");
                pcb_replyBegin(req_info->source_peer, req_info->original_req, reply_firstError(request_reply(req)));
                req_info->reply_started = true;
            }
            const variant_t* val = reply_item_returnValue(item);
            pcb_writeFunctionReturnBegin(req_info->source_peer, req_info->original_req);
            pcb_writeReturnValue(req_info->source_peer, req_info->original_req, val);
            argument_value_list_t* args = reply_item_returnArguments(item);
            if(!llist_isEmpty(args)) {
                pcb_writeReturnArgumentsStart(req_info->source_peer, req_info->original_req);
                pcb_writeReturnArguments(req_info->source_peer, req_info->original_req, args);
            }
            if(!default_sendErrorList(req_info->source_peer, req)) {
                goto exit_reply_error;
            }
            pcb_writeFunctionReturnEnd(req_info->source_peer, req_info->original_req);
        }
        break;
        default:
            SAH_TRACE_WARNING("Not handled reply type !!!");
            break;
        }
        item = reply_nextItem(item);
    }

    SAH_TRACE_OUT();
    return true;

exit_reply_error:
    SAH_TRACE_OUT();
    return false;
}

static bool pcb_sysbus_forwardedDone(request_t* req, pcb_t* pcb, peer_info_t* from, void* userdata) {
    SAH_TRACE_IN();
    // the done handler is called for each child that is finished
    (void) pcb;
    (void) from;

    request_source_info_t* req_info = (request_source_info_t*) userdata;
    if(!req_info) {
        return false;
    }

    if(!request_hasChildren(req)) {
        SAH_TRACE_INFO("All child requests are done there will come no more reply");
        SAH_TRACE_INFO("Original request:");
        SAH_TRACE_INFO("    Object path: %s", request_path(req));
        SAH_TRACE_INFO("    Type       : %d", request_type(req));
        SAH_TRACE_INFO("    Attributes : 0x%X", request_attributes(req));
        if(req_info->objects_inList) {
            pcb_writeObjectListEnd(req_info->source_peer, req);
        }
        if(!(*request_path(req)) && ((request_attributes(req) & request_notify_all) != 0) && (request_type(req) == request_type_get_object)) {
            // it is a get object request with notification flags on the root
            // keep it
            SAH_TRACE_INFO("All child request done, but keep original request");
            req_info->reply_started = false;
            default_sendErrorList(req_info->source_peer, req_info->original_req);
        } else {
            // otherwise stop the request
            SAH_TRACE_INFO("All child request done, stop and destroy the original request");
            if(!(*request_path(req)) && req_info->reply_started && (request_type(req) != request_type_find_objects)) {
                // close the root object
                default_sendErrorList(req_info->source_peer, req_info->original_req);
                pcb_writeObjectEnd(req_info->source_peer, req_info->original_req);
            }
            if(!req_info->reply_started) {
                pcb_replyBegin(req_info->source_peer, req_info->original_req, 0);
                default_sendErrorList(req_info->source_peer, req_info->original_req);
                req_info->reply_started = true;
            }

            SAH_TRACE_INFO("Write end");
            pcb_replyEnd(req_info->source_peer, req_info->original_req);
            request_setCancelHandler(req, NULL);
            request_setReplyHandler(req, NULL);
            request_setDoneHandler(req, NULL);
            request_setDone(req);
            request_destroy(req);
        }
    } else {
        // reset the flag
        req_info->objects_done = false;
        // if the original request was a getObject request with notifications send reply end
        if(((request_attributes(req) & request_notify_all) != 0) && (request_type(req) == request_type_get_object)) {
            SAH_TRACE_INFO("Write end");
            default_sendErrorList(req_info->source_peer, req_info->original_req);
            if(!pcb_replyEnd(req_info->source_peer, req_info->original_req)) {
                SAH_TRACE_OUT();
                return false;
            }
        }
    }
    SAH_TRACE_OUT();

    return true;
}

static void pcb_sysbus_forwardedCancel(request_t* req, void* userdata) {
    SAH_TRACE_INFO("Canceling request");
    // is called for each child request that was canceled
    request_source_info_t* req_info = (request_source_info_t*) userdata;
    if(!req_info) {
        return;
    }

    if(peer_isConnected(req_info->source_peer)) {
        if(!req_info->reply_started) {
            pcb_replyBegin(req_info->source_peer, req, pcb_error_connection_shutdown);
            req_info->reply_started = true;
        }
        if(*request_path(req)) {
            // if the request is not on the root path send error "connection shutdown"
            pcb_writeError(req_info->source_peer, req, pcb_error_connection_shutdown, error_string(pcb_error_connection_shutdown), request_path(req));
        } else {
            // if it is get object request without notification flags on the root object
            if(!(request_attributes(req) & request_notify_all) && (request_type(req) == request_type_get_object)) {
                pcb_writeError(req_info->source_peer, req, pcb_error_connection_shutdown, error_string(pcb_error_connection_shutdown), "Plug-in disconnected");
            }
        }
        // handle the child request as done, call pcb_sysbus_forwardedDone
        pcb_sysbus_forwardedDone(req, NULL, NULL, userdata);
    }

    request_destroy(req);
}

bool pcb_sysbus_forwardRequest(request_t* orig_req, peer_info_t* source, request_t* new_req, peer_info_t* dest) {
    request_source_info_t* req_info = request_data(orig_req);
    if(!req_info) {
        if(pcb_sysbus_setForwardData(orig_req, source) == false) {
            SAH_TRACE_ERROR("Failed to allocate source info");
            return false;
        }
    }

    // the original request will have a destroy handler (set in pcb_sysbus_setForwardData)
    // the child request will have a cancel handler, set by the library. When all child requests are canceled
    // this request must be canceled as well, set the cancel handler
    request_setCancelHandler(orig_req, pcb_sysbus_forwardedCancel);
    // need to set reply handler and reply done handler
    request_setReplyHandler(orig_req, pcb_sysbus_forwardedReply);
    request_setDoneHandler(orig_req, pcb_sysbus_forwardedDone);

    // make sure no object caching is done here
    request_setAttributes(orig_req, request_attributes(orig_req) | request_no_object_caching);
    request_setAttributes(new_req, request_attributes(new_req) | request_no_object_caching);
    request_setAttributes(new_req, request_attributes(new_req) & (~request_getObject_keep_hierarchy));

    SAH_TRACE_INFO("Forwarding request, attributes = 0x%x", request_attributes(new_req));
    if(!request_forward(orig_req, source, new_req, dest)) {
        SAH_TRACE_ERROR("Failed to forward request (0x%X - %s)", pcb_error, error_string(pcb_error));
        return false;
    } else {
        SAH_TRACE_INFO("Forward request request id = %d", request_id(new_req));
    }
    return true;
}

/* The default handler can be used for
   - get request (except on root object)
   - set request
   - create instance request
   - delete instance request
 */
bool pcb_sysbus_defaultHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);
    bool local_only = false;

    pcb_sysbus_sniffer(req);

    const char* dm_namespace = datamodel_namespace(datamodel);
    char* oldpath = strdup(request_path(req));
    if(strncmp(request_path(req), dm_namespace, strlen(dm_namespace)) == 0) {
        local_only = true;
        request_setPath(req, oldpath + strlen(dm_namespace));
        request_setAttributes(req, request_attributes(req) | request_common_include_namespace);
    }

    const char* objectPath = request_path(req);
    uint32_t attributes = request_attributes(req);
    uint32_t depth = request_depth(req);

    peer_info_t* listensocket = peer_listenSocket(peer);
    listen_socket_t* listen_info = peer_getUserData(listensocket);
    SAH_TRACE_INFO("Request type = [%d], object path = [%s], source pid = [%d], user id = [%d (%d)]",
                   request_type(req), request_path(req), request_getPid(req), request_userID(req), listen_info ? listen_info->defaultUid : 0);

    if(listen_info && (listen_info->defaultUid != 0) && (listen_info->defaultUid != UINT32_MAX)) {
        request_setUserID(req, listen_info->defaultUid);
    }

    uint32_t localAttributes = attributes & (path_attr_key_notation | path_attr_slash_seperator);
    localAttributes |= path_attr_partial_match;
    bool exactMatch = false;

    // get the best matching
    object_t* object = object_getObject(root, objectPath, localAttributes, &exactMatch);
    system_object_info_t* info = object_getUserData(object);

    if(!object || (object == root)) {
        SAH_TRACE_NOTICE("Root object found");
        if(attributes & (request_notify_all | request_wait)) {
            SAH_TRACE_INFO("Has notification flags set, store request for later dispatching");
            pcb_sysbus_setForwardData(req, peer);
            request_setPath(req, oldpath);
            free(oldpath);
            return object_addNotifyRequest(root, req);
        } else {
            SAH_TRACE_NOTICE("Return not found error");
            free(oldpath);
            return default_getObjectHandler(peer, datamodel, req);
        }
    } else {
        SAH_TRACE_NOTICE("Object found = %s", object_name(object, path_attr_key_notation));
        if((object_parent(object) == root) || (object_parent(object) == NULL) || (strcmp(object_name(object_parent(object), 0), "Process") == 0)) {
            if(info) {
                system_object_info_t* proc_info = object_getUserData(info->process);
                if(proc_info && proc_info->process && local_only) {
                    if(attributes & (request_notify_all | request_wait)) {
                        SAH_TRACE_INFO("Has notification flags set, store request for later dispatching");
                        pcb_sysbus_setForwardData(req, peer);
                        request_setPath(req, oldpath);
                        free(oldpath);
                        return object_addNotifyRequest(root, req);
                    } else {
                        SAH_TRACE_NOTICE("Object from another bus, should be local");
                        SAH_TRACE_INFO("Object %s not found", objectPath);
                        pcb_replyBegin(peer, req, pcb_error_not_found);
                        pcb_writeError(peer, req, pcb_error_not_found, error_string(pcb_error_not_found), objectPath);
                        SAH_TRACE_INFO("Write end");
                        pcb_replyEnd(peer, req);
                        free(oldpath);
                        return false;
                    }
                }
            }
        } else {
            info = NULL;
        }
    }
    free(oldpath);

    // internal objects are handled as a normal plugin
    // objects without information are also handled as a normal plugin
    if((info && ((info->type == system_object_internal) || (info->type == system_object_self))) || !info) {
        switch(request_type(req)) {
        case request_type_get_object:
            SAH_TRACE_INFO("Using default handler");
            return default_getObjectHandler(peer, datamodel, req);
            break;
        case request_type_set_object:
            SAH_TRACE_INFO("Using default handler");
            return default_setObjectHandler(peer, datamodel, req);
            break;
        case request_type_create_instance:
            SAH_TRACE_INFO("Using default handler");
            return default_createInstanceHandler(peer, datamodel, req);
            break;
        case request_type_delete_instance:
            SAH_TRACE_INFO("Using default handler");
            return default_deleteInstanceHandler(peer, datamodel, req);
            break;
        case request_type_exec_function:
            SAH_TRACE_INFO("Using default handler");
            return default_executeFunctionHandler(peer, datamodel, req);
            break;
        case request_type_find_objects:
            SAH_TRACE_INFO("Using default handler");
            return default_findObjectsHandler(peer, datamodel, req);
            break;
        default:
            SAH_TRACE_INFO("Invalid request");
            pcb_replyBegin(peer, req, pcb_error_not_supported_request);
            pcb_writeError(peer, req, pcb_error_not_supported_request, error_string(pcb_error_not_supported_request), "");
            SAH_TRACE_INFO("Write end");
            pcb_replyEnd(peer, req);
            return false;
            break;
        }
    }

    request_t* copy_req = request_copy(req, depth, attributes | request_no_object_caching, request_copy_default);
    if(info && info->process && (strcmp(object_da_parameterCharValue(info->process, "Type"), "Bus") == 0)) {
        info = (system_object_info_t*) object_getUserData(info->process);
        SAH_TRACE_INFO("Namespace object belongs to a bus, ignore namespace");
    } else {
        SAH_TRACE_INFO("Namespace object => remove namespace");
        if(info->type == system_object_dispatch_namespace) {
            const char* path = request_path(req);
            if(request_attributes(req) & request_common_path_slash_seperator) {
                path = strchr(path, '/');
            } else {
                path = strchr(path, '.');
            }
            if(path) {
                path++;
                request_setPath(copy_req, path);
            } else {
                request_setPath(copy_req, "");
            }
            SAH_TRACE_INFO("Must reply with namespace included");
            request_setAttributes(copy_req, attributes | request_common_include_namespace);
        }
    }
    if(!pcb_sysbus_forwardRequest(req, peer, copy_req, info->peer)) {
        pcb_replyBegin(peer, req, pcb_error);
        pcb_writeError(peer, req, pcb_error, error_string(pcb_error), "");
        SAH_TRACE_INFO("Write end");
        pcb_replyEnd(peer, req);
        request_destroy(copy_req);
        return false;
    }

    if((request_attributes(req) & request_notify_all) != 0) {
        object_addNotifyRequest(object, req);
    }

    if(request_type(req) == request_type_find_objects) {
        request_source_info_t* req_info = request_data(req);
        if(req_info) {
            req_info->reply_started = true;
            req_info->objects_inList = true;
            req_info->first_inList = true;
        }
        pcb_replyBegin(peer, req, pcb_error_not_supported_request);
        pcb_writeObjectListBegin(peer, req, object_list);
    }

    if(request_type(req) == request_type_set_object) {
        request_parameterListClear(req);
    }

    return true;
}

bool pcb_sysbus_getHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    SAH_TRACE_IN();
    object_t* root = datamodel_root(datamodel);

    request_setAttributes(req, request_attributes(req) & (~request_common_include_namespace));

    const char* dm_namespace = datamodel_namespace(datamodel);
    if((strncmp(request_path(req), dm_namespace, strlen(dm_namespace)) == 0) &&
       (strlen(dm_namespace) == strlen(request_path(req)))) {
        char* oldpath = strdup(request_path(req));
        request_setPath(req, oldpath + strlen(dm_namespace));
        request_setAttributes(req, request_attributes(req) | request_common_include_namespace);
        free(oldpath);
    }
    const char* objectPath = request_path(req);
    bool retval = true;

    if(objectPath && *objectPath) {
        retval = pcb_sysbus_defaultHandler(peer, datamodel, req);
        SAH_TRACE_OUT();
        return retval;
    }
    peer_info_t* listensocket = peer_listenSocket(peer);
    listen_socket_t* listen_info = peer_getUserData(listensocket);
    SAH_TRACE_INFO("Request type = [%d], object path = [%s], source pid = [%d], user id [%d]",
                   request_type(req), request_path(req), request_getPid(req), listen_info ? listen_info->defaultUid : 0);

    if(listen_info && (listen_info->defaultUid != 0) && (listen_info->defaultUid != UINT32_MAX)) {
        request_setUserID(req, listen_info->defaultUid);
    }

    request_source_info_t* req_info = NULL;
    if((req_info = pcb_sysbus_setForwardData(req, peer)) == false) {
        SAH_TRACE_ERROR("Failed to allocate source info");
        SAH_TRACE_OUT();
        return false;
    }
    req_info->reply_started = true;

    pcb_replyBegin(peer, req, 0);
    object_t* child = NULL;

    uint32_t depth = request_depth(req);
    uint32_t attributes = request_attributes(req);

    pcb_writeObjectBegin(peer, req, root);
    pcb_writeObjectListBegin(peer, req, object_list_children);
    req_info->objects_inList = true;
    req_info->first_inList = true;
    if((attributes & request_getObject_children) || (depth > 0)) {
        SAH_TRACE_INFO("Looping over all root children");
        object_for_each_child(child, root) {
            system_object_info_t* info = object_getUserData(child);
            if((info && (info->type == system_object_internal)) || !info) {
                retval = pcb_sysbus_handle_internal_object(peer, req, req_info, child);
            } else {
                if(info->process && (strcmp(object_da_parameterCharValue(info->process, "Type"), "Bus") != 0)) {
                    retval = pcb_sysbus_handle_dispatch_object(peer, req, info, child);
                } else {
                    continue;
                }
            }
            if(!retval) {
                break;
            }
        }
        if(retval && (request_state(req) != request_state_forwarded)) {
            pcb_writeObjectListEnd(peer, req);
            default_sendErrorList(peer, req);
            pcb_writeObjectEnd(peer, req);
        }
    } else {
        pcb_writeObjectListEnd(peer, req);
        pcb_writeObjectEnd(peer, req);
    }
    if(retval) {
        if(request_state(req) != request_state_forwarded) {
            SAH_TRACE_INFO("Write end");
            if(pcb_replyEnd(peer, req)) {
                request_setDone(req);
            } else {
                SAH_TRACE_OUT();
                return false;
            }
        }

        if((request_attributes(req) & request_notify_all) != 0) {
            object_addNotifyRequest(root, req);
        }
    }
    SAH_TRACE_OUT();
    return retval;
}

bool pcb_sysbus_findObjectsHandler(peer_info_t* peer, datamodel_t* datamodel, request_t* req) {
    object_t* root = datamodel_root(datamodel);
    const char* objectPath = request_path(req);
    const char* pattern = request_pattern(req);
    uint32_t attributes = request_attributes(req);
    uint32_t depth = request_depth(req);

    if(objectPath && *objectPath) {
        bool retval = pcb_sysbus_defaultHandler(peer, datamodel, req);
        SAH_TRACEZ_OUT("pcb");
        return retval;
    }

    request_source_info_t* req_info = NULL;
    if((req_info = pcb_sysbus_setForwardData(req, peer)) == NULL) {
        SAH_TRACE_ERROR("Failed to allocate source info");
        return false;
    }

    peer_info_t* listensocket = peer_listenSocket(peer);
    listen_socket_t* listen_info = peer_getUserData(listensocket);
    SAH_TRACEZ_INFO("request", "Request type = [%d], object path = [%s], source pid = [%d], user id [%d]",
                    request_type(req), request_path(req), request_getPid(req), listen_info ? listen_info->defaultUid : 0);


    req_info->reply_started = true;
    req_info->objects_inList = true;
    req_info->first_inList = true;

    uint32_t findAttr = attributes & (path_attr_key_notation | path_attr_slash_seperator);

    if(listen_info && (listen_info->defaultUid != 0) && (listen_info->defaultUid != UINT32_MAX)) {
        request_setUserID(req, listen_info->defaultUid);
    }

    char* modified_pattern = NULL;
    int32_t s = asprintf(&modified_pattern, "%s", pattern);
    if(s == -1) {
        pcb_replyBegin(peer, req, pcb_error_no_memory);
        pcb_writeError(peer, req, pcb_error_no_memory, error_string(pcb_error_no_memory), objectPath);
        pcb_replyEnd(peer, req);
        return false;
    }

    pcb_replyBegin(peer, req, 0);

    char* parameter = NULL;
    char* value = NULL;
    if(attributes & (request_find_include_parameters | request_find_include_values)) {
        char* pos = strchr(modified_pattern, '=');
        if(pos) {
            value = pos + 1;
            *pos = 0;
            if(attributes & path_attr_slash_seperator) {
                pos = strrchr(modified_pattern, '/');
            } else {
                pos = strrchr(modified_pattern, '.');
            }
            if(pos) {
                parameter = pos + 1;
                *pos = 0;
            } else {
                attributes &= ~(request_find_include_parameters | request_find_include_values);
            }
        } else {
            attributes &= ~(request_find_include_parameters | request_find_include_values);
        }
    }
    SAH_TRACEZ_INFO("pcb", "path = %s", objectPath);
    SAH_TRACEZ_INFO("pcb", "pattern = %s", modified_pattern);
    SAH_TRACEZ_INFO("pcb", "parameter = %s", parameter);
    SAH_TRACEZ_INFO("pcb", "value = %s", value);

    free(modified_pattern);

    pcb_writeObjectListBegin(peer, req, object_list);
    object_t* child = NULL;
    object_list_t* objects = NULL;
    llist_iterator_t* it = NULL;
    string_t* paramValue = NULL;
    object_t* foundObject = NULL;
    object_for_each_child(child, root) {
        system_object_info_t* info = object_getUserData(child);
        if((info && (info->type == system_object_internal)) || !info) {
            objects = object_findObjects(child, pattern, depth, findAttr);
            llist_for_each(it, objects) {
                foundObject = object_item(it);
                SAH_TRACEZ_INFO("pcb", "Object path matches");
                if(attributes & (request_find_include_parameters | request_find_include_values)) {
                    SAH_TRACE_INFO("Checking parameter %s", parameter);
                    paramValue = object_parameterStringValue(foundObject, parameter);
                    if(!paramValue) {
                        SAH_TRACE_INFO("Parameter not found");
                        continue;
                    }
                    SAH_TRACE_INFO("Value = %s", string_buffer(paramValue));
#if defined(FNM_EXTMATCH)
                    if(fnmatch(value, string_buffer(paramValue), FNM_CASEFOLD | FNM_EXTMATCH) != 0) {
#else
                    if(fnmatch(value, string_buffer(paramValue), FNM_CASEFOLD) != 0) {
#endif
                        string_cleanup(paramValue);
                        free(paramValue);
                        continue;
                    }
                }
                if(req_info->first_inList) {
                    req_info->first_inList = false;
                } else {
                    pcb_writeObjectListNext(peer, req);
                }
                default_reply_object(peer, foundObject, 0, attributes, req, request_parameterList(req));
            }
            llist_cleanup(objects);
            free(objects);
            objects = NULL;
        } else {
            request_t* copy_req = request_copy(req, depth, attributes | request_no_object_caching, request_copy_default);
            request_setPath(copy_req, object_name(child, attributes & (request_common_path_key_notation | request_common_path_slash_seperator)));
            if(depth != UINT32_MAX) {
                request_setDepth(copy_req, depth - 1);
            }
            pcb_sysbus_forwardRequest(req, peer, copy_req, info->peer);
        }
    }
    if(request_state(req) != request_state_forwarded) {
        pcb_writeObjectListEnd(peer, req);
        default_sendErrorList(peer, req);
        if(pcb_replyEnd(peer, req)) {
            request_setDone(req);
        } else {
            return false;
        }
    }

    return true;
}
