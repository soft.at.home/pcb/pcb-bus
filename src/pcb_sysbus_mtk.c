/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "pcb_sysdatamodel.h"
#include "parse_args.h"
#include "pcb_sysbus_mtk.h"

#define NOTIFY_MTK_RPC 110

extern application_config_t appconfig;

static bool mtk_rpc_notify(mtk_rpc_t* rpc, system_object_info_t* info, argument_value_list_t* args) {
    bool ok = false;

    notification_t* notification = NULL;

    notification = notification_create(NOTIFY_MTK_RPC);
    notification_setName(notification, function_name(fcall_function(rpc->fcall)));

    if(rpc->so) {
        notification_addParameter(notification, notification_parameter_create("so", object_name(rpc->so->object, path_attr_key_notation)));
    }
    if(rpc->module) {
        notification_addParameter(notification, notification_parameter_create("module", object_name(rpc->module->object, path_attr_key_notation)));
    }

    variant_t number;
    variant_initialize(&number, variant_type_uint32);
    variant_setUInt32(&number, rpc->id);
    notification_addParameter(notification, notification_parameter_createVariant("id", &number));
    variant_setUInt32(&number, request_attributes(fcall_request(rpc->fcall)));
    notification_addParameter(notification, notification_parameter_createVariant("attributes", &number));
    variant_cleanup(&number);

    if(args) {
        argument_value_t* arg;
        for(arg = argument_valueFirstArgument(args); arg; arg = argument_valueNextArgument(arg)) {
            notification_addParameter(notification, notification_parameter_createVariant(argument_valueName(arg),
                                                                                         argument_value(arg)));
        }
    }

    SAH_TRACE_INFO("Sending notification ...");
    if(!pcb_sendNotification(appconfig.pcb, info->peer, NULL, notification)) {
        SAH_TRACE_ERROR("Failed to send notification: %d (%s)", pcb_error, error_string(pcb_error));
        goto leave;
    }

    ok = true;

leave:
    if(notification) {
        notification_destroy(notification);
    }
    return ok;
}

static mtk_rpc_t* mtk_rpc_find(system_object_info_t* info, uint32_t id) {
    llist_iterator_t* it = NULL;
    mtk_rpc_t* rpc = NULL;
    llist_for_each(it, &info->rpcs) {
        rpc = llist_item_data(it, mtk_rpc_t, it);
        if(rpc->id == id) {
            return rpc;
        }
    }

    return NULL;
}

static void mtk_rpc_destroy(mtk_rpc_t* rpc) {
    if(!rpc) {
        return;
    }
    llist_iterator_take(&rpc->it);
    if(rpc->fcall) {
        fcall_setUserData(rpc->fcall, NULL);
        fcall_destroy(rpc->fcall);
    }
    free(rpc);
}

static mtk_rpc_t* mtk_rpc_create(system_object_info_t* info, mtk_so_t* so, mtk_module_t* module, function_call_t* fcall) {
    mtk_rpc_t* rpc = calloc(1, sizeof(mtk_rpc_t));
    if(!rpc) {
        SAH_TRACE_ERROR("calloc failed");
        return NULL;
    }
    rpc->id = info->rpcid++;
    rpc->so = so;
    rpc->module = module;
    rpc->fcall = fcall;
    llist_append(&info->rpcs, &rpc->it);
    return rpc;
}

static void mtk_module_destroy(mtk_module_t* module) {
    if(!module) {
        return;
    }

    system_object_info_t* info = object_getUserData(module->so->process);

    mtk_process_cancelRPCs(info, module, "Module is unloaded", object_name(module->object, path_attr_key_notation));
    llist_iterator_take(&module->it);
    if(module->object) {
        object_setUserData(module->object, NULL);
        object_parameterSetBoolValue(module->object, "Registered", false);
        object_commit(module->object);
    }
    free(module);
}

static mtk_module_t* mtk_module_find(mtk_so_t* so, const char* name) {
    mtk_module_t* module;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &so->modules) {
        module = llist_item_data(it, mtk_module_t, it);
        if(strcmp(object_name(module->object, path_attr_key_notation), name) == 0) {
            return module;
        }
    }

    return NULL;
}

static void mtk_module_objectDestroy(object_t* object) {
    mtk_module_t* module = (mtk_module_t*) object_getUserData(object);
    if(module) {
        object_setUserData(object, NULL);
        free(module);
    }
}

static mtk_module_t* mtk_module_create(mtk_so_t* so, const char* name) {
    mtk_module_t* module = mtk_module_find(so, name);
    object_t* parent = NULL;
    if(module && object_parameterBoolValue(module->object, "Registered")) {
        SAH_TRACE_WARNING("Module %s already registered for SharedObject %s", name, object_name(so->object, path_attr_key_notation));
        return NULL;
    }
    if(!module) {
        module = calloc(1, sizeof(mtk_module_t));
    }
    if(!module) {
        SAH_TRACE_ERROR("calloc() failed");
        goto error;
    }
    llist_append(&so->modules, &module->it);
    module->so = so;
    parent = object_getObjectByKey(so->object, "Module");
    module->object = object_getObjectByKey(parent, name);
    if(!module->object) {
        module->object = object_createInstance(parent, 0, name);
    }
    if(!module->object) {
        SAH_TRACE_ERROR("Could not create object instance for module \"%s\"", name);
        goto error;
    }
    module->type = system_object_internal;
    object_setUserData(module->object, module);
    object_setDestroyHandler(module->object, mtk_module_objectDestroy);
    object_parameterSetBoolValue(module->object, "Registered", true);
    object_commit(module->object);
    return module;
error:
    mtk_module_destroy(module);
    return NULL;
}

static void mtk_so_destroy(mtk_so_t* so) {
    if(!so) {
        return;
    }
    while(!llist_isEmpty(&so->modules)) {
        llist_iterator_t* it = llist_last(&so->modules);
        mtk_module_t* module = llist_item_data(it, mtk_module_t, it);
        mtk_module_destroy(module);
    }

    if(so->object) {
        object_setUserData(so->object, NULL);
        object_parameterSetBoolValue(so->object, "Registered", false);
        object_commit(so->object);
    }

    llist_iterator_take(&so->it);
    free(so);
}

static mtk_so_t* mtk_so_find(system_object_info_t* info, const char* name) {
    mtk_so_t* so = NULL;
    llist_iterator_t* it = NULL;
    llist_for_each(it, &info->sos) {
        so = llist_item_data(it, mtk_so_t, it);
        if(!strcmp(object_name(so->object, path_attr_key_notation), name)) {
            return so;
        }
    }

    return NULL;
}

static void mtk_so_objectDestroy(object_t* object) {
    mtk_so_t* so = (mtk_so_t*) object_getUserData(object);
    if(so) {
        object_setUserData(object, NULL);
        free(so);
    }
}

static mtk_so_t* mtk_so_create(object_t* process, const char* name, const char* path) {
    system_object_info_t* info = object_getUserData(process);
    object_t* parent = NULL;

    mtk_so_t* so = mtk_so_find(info, name);
    if(so && object_parameterBoolValue(so->object, "Registered")) {
        SAH_TRACE_WARNING("SharedObject %s already registered for Process %s", name, object_name(process, path_attr_key_notation));
        return NULL;
    }

    if(!so) {
        so = calloc(1, sizeof(mtk_so_t));
    }
    if(!so) {
        SAH_TRACE_ERROR("calloc() failed");
        goto error;
    }
    llist_append(&info->sos, &so->it);
    so->process = process;

    parent = object_getObjectByKey(process, "SharedObject");
    so->object = object_getObjectByKey(parent, name);
    if(!so->object) {
        so->object = object_createInstance(parent, 0, name);
    }
    if(!so->object) {
        SAH_TRACE_ERROR("Could not create object instance for so \"%s\"", name);
        goto error;
    }
    so->type = system_object_internal;
    object_setUserData(so->object, so);
    object_parameterSetBoolValue(so->object, "Registered", true);
    object_parameterSetCharValue(so->object, "Path", path);
    object_setDestroyHandler(so->object, mtk_so_objectDestroy);
    object_commit(so->object);
    return so;
error:
    mtk_so_destroy(so);
    return NULL;
}

static void mtk_rpc_reply(mtk_rpc_t* rpc, bool ok, variant_t* returnValue, argument_value_list_t* args) {
    peer_info_t* peer = fcall_peer(rpc->fcall);
    request_t* request = fcall_request(rpc->fcall);

    pcb_replyBegin(peer, request, ok ? 0 : pcb_error_function_exec_failed);
    if(!ok && !reply_hasErrors(request_reply(request))) {
        reply_addError(request_reply(request), pcb_error_function_exec_failed, error_string(pcb_error_function_exec_failed), NULL);
    }
    pcb_writeFunctionReturn(rpc->fcall, returnValue, args);
}

static void mtk_rpc_handleFcallCanceled(function_call_t* fcall, void* userdata) {
    (void) fcall;
    mtk_rpc_t* rpc = userdata;
    if(!rpc) {
        return;
    }
    rpc->fcall = NULL;
    mtk_rpc_destroy(rpc);
}

static function_exec_state_t mtk_rpc_handleFcall(system_object_info_t* info, mtk_so_t* so, mtk_module_t* module,
                                                 function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    (void) retval;
    function_exec_state_t fstate = function_exec_error;
    request_t* request = fcall_request(fcall);
    mtk_rpc_t* rpc = NULL;
    const char* name = object_name(fcall_object(fcall), path_attr_key_notation);
    if(!info || !object_parameterBoolValue(fcall_object(fcall), "Registered")) {
        SAH_TRACE_ERROR("No information availalbe (%p)", info);
        reply_addError(request_reply(request), pcb_error_not_found, "Process or module not registered", name);
        goto leave;
    }

    rpc = mtk_rpc_create(info, so, module, fcall);
    if(!rpc) {
        reply_addError(request_reply(request), pcb_error_invalid_name, "RPC creation failed", name);
        goto leave;
    }

    fcall_setUserData(fcall, rpc);
    fcall_setCancelHandler(fcall, mtk_rpc_handleFcallCanceled);
    if(!mtk_rpc_notify(rpc, info, args)) {
        reply_addError(request_reply(request), pcb_error_communication, "Failed to send RPC request", name);
        goto leave;
    }
    fstate = function_exec_executing;

leave:
    if(fstate == function_exec_error) {
        if(rpc) {
            mtk_rpc_destroy(rpc);
        } else {
            fcall_destroy(fcall);
        }
    }

    return fstate;
}

static function_exec_state_t mtk_rpc_handleProcessFcall(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_INFO("Calling function %s for process %s", function_name(fcall_function(fcall)), object_name(fcall_object(fcall), path_attr_key_notation));
    system_object_info_t* info = object_getUserData(fcall_object(fcall));
    return mtk_rpc_handleFcall(info, NULL, NULL, fcall, args, retval);
}

static function_exec_state_t mtk_rpc_handleModuleFcall(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    mtk_module_t* module = object_getUserData(fcall_object(fcall));
    mtk_so_t* so = module ? module->so : NULL;
    object_t* process = so ? so->process : NULL;
    system_object_info_t* info = object_getUserData(process);
    return mtk_rpc_handleFcall(info, so, module, fcall, args, retval);
}

static function_exec_state_t mtk_rpc_rpcFinish(function_call_t* fcall, argument_value_list_t* args, variant_t* _retval) {
    (void) _retval;
    function_exec_state_t fstate = function_exec_error;
    uint32_t id = 0;
    argument_value_t* retval = NULL;
    bool ok = true;
    variant_list_t* errors = NULL;
    variant_list_iterator_t* it;
    variant_map_t* error;
    mtk_rpc_t* rpc = NULL;

    request_t* request = fcall_request(fcall);
    system_object_info_t* info = object_getUserData(fcall_object(fcall));
    if(!info || !info->peer) {
        reply_addError(request_reply(request), pcb_error_not_found, "Process not registered or disconnected",
                       object_name(fcall_object(fcall), path_attr_key_notation));
        goto leave;
    }

    argument_getUInt32(&id, args, request_attributes(request), "id", 0);
    argument_getBool(&ok, args, request_attributes(request), "ok", true);
    argument_get(&retval, args, request_attributes(request), "retval");
    argument_getList(&errors, args, request_attributes(request), "errors", NULL);

    rpc = mtk_rpc_find(info, id);
    if(!rpc) {
        reply_addError(request_reply(request), pcb_error_not_found, "No such pending RPC",
                       object_name(fcall_object(fcall), path_attr_key_notation));
        goto leave;
    }

    variant_list_for_each(it, errors) {
        error = variant_da_map(variant_list_iterator_data(it));
        if(!error) {
            SAH_TRACE_WARNING("Error list should be a list of error maps");
            break;
        }
        uint32_t code = variant_uint32(variant_map_iterator_data(variant_map_find(error, "code")));
        char* description = variant_char(variant_map_iterator_data(variant_map_find(error, "description")));
        char* info = variant_char(variant_map_iterator_data(variant_map_find(error, "info")));
        reply_addError(request_reply(fcall_request(rpc->fcall)), code, description ? description : error_string(code), info ? info : "");
        free(description);
        free(info);
    }

    mtk_rpc_reply(rpc, ok, argument_value(retval), args);
    mtk_rpc_destroy(rpc);

    fstate = function_exec_done;
leave:
    if(retval) {
        argument_valueDestroy(retval);
    }
    if(errors) {
        variant_list_cleanup(errors);
        free(errors);
    }
    fcall_destroy(fcall);
    return fstate;
}

static function_exec_state_t mtk_registerSharedObject(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    (void) retval;
    function_exec_state_t fstate = function_exec_error;
    request_t* request = fcall_request(fcall);
    object_t* process = fcall_object(fcall);
    system_object_info_t* info = object_getUserData(process);

    char* name = NULL;
    char* path = NULL;
    const char* mname = NULL;
    const char* fname = NULL;
    mtk_so_t* so = NULL;
    mtk_module_t* module = NULL;
    variant_map_t* modules = NULL;
    variant_map_t* modulemap = NULL;
    variant_list_t* functions;
    variant_map_iterator_t* it;
    variant_list_iterator_t* jt;
    function_t* function;

    if(!info || !object_parameterBoolValue(process, "Registered")) {
        reply_addError(request_reply(request), pcb_error_not_found, "Process not registered",
                       object_name(fcall_object(fcall), path_attr_key_notation));
        goto leave;
    }

    argument_getChar(&name, args, request_attributes(request), "name", NULL);
    argument_getChar(&path, args, request_attributes(request), "path", NULL);

    so = mtk_so_find(info, name);
    if(!so) {
        so = mtk_so_create(process, name, path);
    }
    if(!so) {
        reply_addError(request_reply(request), pcb_error_invalid_name, "SharedObject registration failed", name);
        goto leave;
    }

    if(!argument_getMap(&modules, args, request_attributes(request), "modules", NULL)) {
        reply_addError(request_reply(request), pcb_error_invalid_value, "Mandatory argument missing", "modules");
        goto leave;
    }

    variant_map_for_each(it, modules) {
        mname = variant_map_iterator_key(it);
        module = mtk_module_find(so, mname) ? : mtk_module_create(so, mname);
        if(!module) {
            reply_addError(request_reply(request), pcb_error_invalid_name, "Module registration failed", mname);
            continue;
        }
        modulemap = variant_da_map(variant_map_iterator_data(it));
        functions = variant_da_list(variant_map_iterator_data(variant_map_find(modulemap, "functions")));
        if(functions) {
            variant_list_for_each(jt, functions) {
                fname = variant_da_char(variant_list_iterator_data(jt));
                function = object_getFunction(module->object, fname);
                if(!function) {
                    function = function_create(module->object, fname, function_type_void, function_attr_variadic);
                }
                if(!function) {
                    reply_addError(request_reply(request), pcb_error_invalid_name, "Function registration failed", fname);
                } else {
                    function_setHandler(function, mtk_rpc_handleModuleFcall);
                }
            }
        }
    }
    fstate = function_exec_done;

    object_commit(process);

leave:
    if(fstate == function_exec_error) {
        mtk_so_destroy(so);
    }

    variant_map_cleanup(modules);
    free(modules);
    free(path);
    free(name);
    fcall_destroy(fcall);
    return fstate;
}

static function_exec_state_t mtk_unregisterSharedObject(function_call_t* fcall, argument_value_list_t* args, variant_t* retval) {
    SAH_TRACE_IN();
    (void) retval;
    function_exec_state_t fstate = function_exec_error;
    object_t* process = fcall_object(fcall);
    SAH_TRACE_INFO("Process = %s", object_name(process, path_attr_key_notation));
    system_object_info_t* info = object_getUserData(process);
    request_t* request = fcall_request(fcall);
    char* name = NULL;

    mtk_so_t* so;
    if(!info || !object_parameterBoolValue(process, "Registered")) {
        reply_addError(request_reply(request), pcb_error_not_found, "Process not registered",
                       object_name(fcall_object(fcall), path_attr_key_notation));
        goto leave;
    }

    argument_getChar(&name, args, request_attributes(request), "name", NULL);

    so = mtk_so_find(info, name);
    if(!so) {
        reply_addError(request_reply(request), pcb_error_not_found, "SharedObject not registered", name);
        goto leave;
    }

    mtk_so_destroy(so);
    object_commit(process);

    fstate = function_exec_done;
leave:
    free(name);
    fcall_destroy(fcall);
    SAH_TRACE_OUT();
    return fstate;
}

bool mtk_process_createDefinition(pcb_t* pcb) {
    if(!pcb) {
        return false;
    }

    object_t* root = datamodel_root(pcb_datamodel(pcb));

    // Fetch Processes object
    object_t* processes = object_getObjectByKey(root, "Process", object_attr_template | object_attr_persistent | object_attr_read_only);
    if(!processes) {
        SAH_TRACE_ERROR("Object not found 'Processes'");
        return false;
    }
    object_aclSet(processes, UINT32_MAX, 0);


    parameter_t* param = parameter_create(processes, "EnableSync", parameter_type_bool, parameter_attr_persistent);
    if(!param) {
        SAH_TRACE_ERROR("Failed to create patameter 'EnableSync'");
        object_rollback(root);
        return false;
    }
    parameter_setWriteHandler(param, pcb_sysbus_processSyncParameterChanged);

    object_t* sharedObject = object_create(processes, "SharedObject", object_attr_template | object_attr_persistent | object_attr_read_only);
    if(!sharedObject) {
        SAH_TRACE_ERROR("Failed to create patameter 'SharedObject'");
        object_rollback(root);
        return false;
    }

    if(!parameter_create(sharedObject, "Registered", parameter_type_bool, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Registered'");
        object_rollback(root);
        return false;
    }

    if(!parameter_create(sharedObject, "Path", parameter_type_string, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Path'");
        object_rollback(root);
        return false;
    }

    object_t* module = object_create(sharedObject, "Module", object_attr_template | object_attr_persistent);
    if(!module) {
        SAH_TRACE_ERROR("Failed to create object 'Module'");
        object_rollback(root);
        return false;
    }

    if(!parameter_create(module, "Enable", parameter_type_bool, parameter_attr_persistent)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Enable'");
        object_rollback(root);
        return false;
    }
    object_parameterSetBoolValue(module, "Enable", true);

    if(!parameter_create(module, "Registered", parameter_type_bool, parameter_attr_read_only)) {
        SAH_TRACE_ERROR("Failed to create patameter 'Registered'");
        object_rollback(root);
        return false;
    }

    // add function getEnv
    function_t* fn = function_create(processes, "getEnv", function_type_string, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleProcessFcall);
    argument_create(fn, "name", argument_type_string, 0);

    // add function setEnv
    fn = function_create(processes, "setEnv", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleProcessFcall);
    argument_create(fn, "name", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "value", argument_type_string, argument_attr_mandatory);

    // add function registerSharedObject
    // void registerSharedObject(mandatory in string name, in string path);
    fn = function_create(processes, "registerSharedObject", function_type_void, function_attr_default | function_attr_variadic);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_registerSharedObject);
    argument_create(fn, "name", argument_type_string, argument_attr_mandatory);
    argument_create(fn, "path", argument_type_string, argument_attr_default);

    // add function unregisterSharedObject
    // void unregisterSharedObject(mandatory in string name);
    fn = function_create(processes, "unregisterSharedObject", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_unregisterSharedObject);
    argument_create(fn, "name", argument_type_string, argument_attr_mandatory);

    // add function rpcFinish
    // void rpcFinish(mandatory in uint32 id, mandatory in string ok, in string retval, in list errors, ...);
    fn = function_create(processes, "rpcFinish", function_type_void, function_attr_default | function_attr_variadic);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_rpcFinish);
    argument_create(fn, "id", argument_type_uint32, argument_attr_mandatory);
    argument_create(fn, "ok", argument_type_bool, argument_attr_mandatory);
    argument_create(fn, "retval", argument_type_string, argument_attr_default);
    argument_create(fn, "errors", argument_type_list, argument_attr_default);

    // void loadSharedObject(mandatory in string sofile);
    fn = function_create(processes, "loadSharedObject", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleProcessFcall);
    argument_create(fn, "sofile", argument_type_string, argument_attr_mandatory);

    // void unloadSharedObject(mandatory in string name);
    fn = function_create(processes, "unloadSharedObject", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleProcessFcall);
    argument_create(fn, "name", argument_type_string, argument_attr_mandatory);

    // void start();
    fn = function_create(module, "start", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleModuleFcall);

    // void stop();
    fn = function_create(module, "stop", function_type_void, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleModuleFcall);

    // bool isRunning();
    fn = function_create(module, "isRunning", function_type_bool, function_attr_default);
    if(!fn) {
        object_rollback(root);
        return false;
    }
    function_setHandler(fn, mtk_rpc_handleModuleFcall);

    return true;
}

void mtk_process_cancelRPCs(system_object_info_t* proc_info, mtk_module_t* module, const char* description, const char* info) {
    mtk_rpc_t* rpc, * nextrpc;
    for(rpc = (mtk_rpc_t*) llist_first(&proc_info->rpcs); rpc; rpc = nextrpc) {
        nextrpc = (mtk_rpc_t*) llist_iterator_next(&rpc->it);
        if(module && (rpc->module != module)) {
            continue;
        }
        reply_addError(request_reply(fcall_request(rpc->fcall)), pcb_error_connection_shutdown, description, info);
        mtk_rpc_reply(rpc, function_exec_error, NULL, NULL);
        mtk_rpc_destroy(rpc);
    }
}

void mtk_process_unregisterSharedObject(object_t* sharedObject) {
    mtk_so_t* so = (mtk_so_t*) object_getUserData(sharedObject);

    mtk_so_destroy(so);
}
