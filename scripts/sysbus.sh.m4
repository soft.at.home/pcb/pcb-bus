include(STAGINGDIR/components.config.m4)dnl
#!/bin/sh
PIDSYSBUS="/var/run/sysbus.pid"

. /usr/bin/pcb_common.sh

name="CONFIG_PCB_SYSBUS_NAME"
ifdef(`CONFIG_PCB_SYSBUS_TCP', ifdef(`CONFIG_PCB_SYSBUS_TCP_PORT',start_options="--priority=-1 -A 0.0.0.0 -P CONFIG_PCB_SYSBUS_TCP_PORT",start_options="--priority=-16 -A 0.0.0.0 -P 7000"),start_options="--priority=-16")
component="sah_services_pcb-bus"

SER="libpcb_serialize_ddw.so,libpcb_serialize_http.so,libpcb_serialize_odl.so"

start() {
    if pcb_is_running $name; then
        echo "sysbus already started"
    else
        PCB_SERIALIZERS=${SER} pcb_sysbus -vv -n $name $start_options
    fi
}

case $1 in
    boot|start)
        start
    ;;
    stop|shutdown)
        pcb_stop $name
    ;;
    debuginfo)
        pcb_debug_info $name $component Debug Bus
        ;;
    *)
        echo "Usage : $0 [boot|start|stop|shutdown|debuginfo]"
        ;;
esac
