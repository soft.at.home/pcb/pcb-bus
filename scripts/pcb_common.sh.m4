sinclude(STAGINGDIR/components.config.m4)dnl
#!/bin/sh

BUS_NAME=ifdef(`CONFIG_PCB_SYSBUS_NAME', CONFIG_PCB_SYSBUS_NAME, "sysbus")
LXC_PROCES_FILE="/etc/config/lxc_process_file"
export LXC_PATH="CONFIG_SAH_SERVICES_LXC_PATH"

VERSION_FILE="/web/version.txt"
if [ ! -f /web/version.txt ]; then
    VERSION_FILE="/version.txt"
fi

running_pid()
{
    pid="$1"
    name="$2"
    index="$3"
    [ -z "$pid" ] && return 1
    [ ! -d /proc/"$pid" ] &&  return 1
    cmd=$(tr '\000' ' ' < /proc/"$pid"/cmdline | tr -s ' ' | cut -d ' ' -f "$index")
    [ "$cmd" != "$name" ] &&  return 1
    return 0
}

show_cmd()
{
    echo "[$(date)]: $*"
    "$@"
}

#usage: pcb_is_running <app_name>
app_is_running()
{
    name="$1"
    shift 1

    pidfile="/var/run/$name.pid"
    if [ ! -f "$pidfile" ]; then
      pidfile="/var/run/$name/$name.pid"
      if [ ! -f "$pidfile" ]; then
        return 1
      fi
    fi
    pid=$(cat "$pidfile")
    running_pid "$pid" "$name" 1 || return 1
    return 0
}


#usage: pcb_process_is_local <app_name>
pcb_process_is_local()
{
    name="$1"

    [ -n "$NOCTR" ] && return 0

    [ -e "/usr/lib/lxc/root/$name.svc" ] && return 1

    return 0
}


lxc_update_link_remove() {
    name="$1"
    rm -f "${LXC_PATH}/$name"
}

lxc_update_link() {
    name="$1"
    lxc_update_link_remove "$name"

    mkdir -p ${LXC_PATH}
    ifelse(CONFIG_SAH_SERVICES_RUI_PACKAGING, `y',`
    application_rui_mounted=$(package info $name | grep "Mounted" | grep -i "yes")
    if [ -n "$application_rui_mounted" ]; then
        ln -s "$(package info $name | grep "Mount point" |  cut -f2 -d: |  cut -f2 -d\ )/$name" "${LXC_PATH}/$name"
    else
        ln -s "/usr/lib/lxc/root/$name.svc" $"{LXC_PATH}/$name"
    fi',`ln -s "/usr/lib/lxc/root/$name.svc" "${LXC_PATH}/$name"')
}


#usage: pcb_is_running_local <app_name>
pcb_is_running_local()
{
    name="$1"
    shift 1

    pidfile="/var/run/$name.pid"
    if [ ! -f "$pidfile" ]; then
      pidfile="/var/run/$name/$name.pid"
      if [ ! -f "$pidfile" ]; then
        return 1
      fi
    fi
    pid=$(cat "$pidfile")
    [ ! -d "/proc/$pid" ] &&  return 1
    cmd=$(tr "\000" " " < "/proc/$pid/cmdline" | tr -s " ")
    index=1;
    pos=0;
    for i in $cmd; do
        if [ "$i" = "-n" ]; then
            pos=$((index+1))
        fi
        index=$((index+1))
    done;
    [ "$pos" = "0" ] && return 1
    running_pid "$pid" "$name" "$pos" || return 1
    return 0
}

#usage: pcb_getpid_local <app_name>
pcb_getpid_local()
{
    name="$1"

    if ! pcb_is_running_local "$name"; then
        return 1
    fi

    pidfile="/var/run/$name.pid"
    if [ ! -f "$pidfile" ]; then
      pidfile="/var/run/$name/$name.pid"
      if [ ! -f "$pidfile" ]; then
        return 1
      fi
    fi

    cat "$pidfile"
}

#usage: pcb_is_running_lxc <app_name>
pcb_is_running_lxc()
{
    name="$1"
    shift 1

    #you cannot use the pid itself as it is the pid in withing the container
    status=$(lxc-info -n "$name" 2>&1 | grep "RUNNING")

    [ -n "$status" ] && return 0

    return 1
}

#usage: pcb_is_running <app_name>
pcb_is_running()
{
    name="$1"

    res=0
    if pcb_process_is_local "$name"; then
        res=$(pcb_is_running_local "$@")
    else
        res=$(pcb_is_running_lxc "$@")
    fi
    return $res
}


#usage: pcb_start_local <app_name> [<other arguments>]
process_start_local()
{
    name="$1"
    shift 1
    if pcb_is_running_local "$name"; then
        #already running
        echo "$name already started"
    else
        #start process here
        "$@"
    fi
}

#usage: pcb_start_lxc <app_name> [<other arguments>]
process_start_lxc()
{
    name="$1"
    shift 1

    if pcb_is_running_lxc "$name"; then
        #already running
        echo "$name already started"
    else
        lxc_update_link "$name"
        #start pcb_app here
        lxc-execute -n "$name" -q -f "${LXC_PATH}/$name/config" -- "$@" &
    fi
}

#usage: process_start <ctr_name> <start_cmd> [<other arguments>]
# if a container exists with name 'ctr_name' then the process will
# be started in a container, otherwise it will run local
process_start()
{
    ctr_name="$1"
    shift 1
    if pcb_process_is_local "$ctr_name"; then
        process_start_local "$ctr_name" "$@"
    else
        process_start_lxc "$ctr_name" "$@"
    fi

}

#usage: pcb_start <app_name> [<other arguments>]
pcb_start()
{
    process_start $1 $pcb_app_preload pcb_app -vv -n "$@"
}

#usage: pcb_stop_local <app_name>
pcb_stop_local()
{
    name="$1"
    shift 1

    pid=$(pcb_getpid_local "$name")
    if [ $? = 0 ]; then
        #stop pcb_app here
        kill "$pid"
    else
        #not running
        echo "$name not running"
    fi
}

#usage: pcb_stop_lxc <app_name>
pcb_stop_lxc()
{
    name="$1"
    shift 1

    if pcb_is_running_lxc "$name"; then
        #stop pcb_app here
        lxc-stop -t 5 -n "$name"
    else
        #not running
        echo "$name not running"
    fi
}

#usage: pcb_stop <app_name>
process_stop()
{
    name="$1"

    if pcb_process_is_local "$name"; then
        pcb_stop_local "$@"
    else
        pcb_stop_lxc "$@"
    fi
}

#usage: pcb_stop <app_name>
pcb_stop()
{
    process_stop "$@"
}

#usage: pcb_hardstop_local <app_name>
pcb_hardstop_local()
{
    name="$1"
    timeout=5

    pid=$(pcb_getpid_local "$name")
    if [ $? = 0 ]; then
        #stop pcb_app here
        kill "$pid"

        #wait for process termination
        while [ $timeout -gt 0 ]; do
          if ! kill -0 "$pid" 2> /dev/null; then
             break
          fi

          sleep 1

          timeout=$((timeout - 1))
        done

        if [ $timeout -eq 0 ]; then
            #process was not cooperative. kill it
            echo "Force process termination of pid $pid"
            kill -9 "$pid"
        fi
    else
        #not running
        echo "$name not running"
    fi
}

#usage: pcb_hardstop <app_name>
pcb_hardstop()
{
    name="$1"

    if pcb_process_is_local "$name"; then
        pcb_hardstop_local "$@"
    else
        pcb_stop_lxc "$@"
    fi
}

#usage: pcb_restart <app_name> [<other arguments>]
pcb_restart()
{
    pcb_hardstop "$@"
    pcb_start "$@"
}

#usage: pcb_debug_info <app_name> <component_name> [<root obj 1> <root obj 2> ....]
pcb_debug_info()
{
    name="$1"
    component="$2"
    shift 2
    datamodel="$*"

    # If GET_DEBUG is 1 this script is called from getDebugInformation. Then
    # do not not dump version info and the data model: getDebugInformation
    # already does that.
    if [ "$GET_DEBUG" != "1" ]; then
        echo "$name version:"
        echo "=============================================="
        head -n 15 $VERSION_FILE
        grep "$component=" $VERSION_FILE
        echo "=============================================="
        echo
    fi

    echo "$name process object"
    echo "=============================================="
    if [ "$name" = "${BUS_NAME}" ]; then
        pcb_cli "Process.$name?"
    else
        pcb_cli -q "Process.${BUS_NAME}_$name.EnableSync=1"
        pcb_cli "Process.${BUS_NAME}_$name?"
    fi
    echo "=============================================="
    echo

    if pcb_is_running "$name"; then
        pidfile="/var/run/$name.pid"
        pid=$(cat "$pidfile")

        echo "$name status:"
        echo "=============================================="
        tr "\000" " " < "/proc/$pid/cmdline" | tr -s " "
        echo
        show_cmd cat "/proc/$pid/status"
        echo "=============================================="
        echo
        if [ "$GET_DEBUG" != "1" ]; then
            if [ -n "$datamodel" ]; then
                echo "$name data model"
                echo "=============================================="
                for i in $datamodel; do
                    pcb_cli "$i?"
                done
                echo "=============================================="
            fi
        fi
    else
        echo "$name status: not running"
    fi
}

#usage: pcb_log <app_name> <enable|disable|reset> <zone1> [<zone2> <zone3> ... <zoneN>]
pcb_log()
{
    name="$1"
    action="$2"
    shift 2

    case "$action" in
        enable)
            pcb_cli -q "Process.${BUS_NAME}_$name.Tracing.Enabled=1"
            pcb_cli -q "Process.${BUS_NAME}_$name.Tracing.TraceLevel=500"
            for zone in "$@"; do
                pcb_cli -q "Process.${BUS_NAME}_$name.addTraceZone(\"$zone\", 500)"
            done
            ;;
        disable)
            pcb_cli -q "Process.${BUS_NAME}_$name.Tracing.Enabled=0"
            ;;
        reset)
            pcb_cli -q "Process.${BUS_NAME}_$name.Tracing.Enabled=1"
            pcb_cli -q "Process.${BUS_NAME}_$name.Tracing.TraceLevel=200"
            for zone in "$@"; do
                pcb_cli -q "Process.${BUS_NAME}_$name.Tracing.${zone}=0"
            done
            ;;
    esac
}

#usage: pcb_is_available <object> <timeout>
pcb_is_available()
{
    object="$1"
    timeout="$2"
    shift 2

    if [ -n "$timeout" ]; then
        data=$(pcb_cli -w "$timeout" "$object?0")
    else
        data=$(pcb_cli -w -1 "$object?0")
    fi
    [ -n "$data" ] && return 0

    return 1
}

#usage: mtk_load <app_name> <so_file>
mtk_load()
{
    name="$1"
    shared_object="$2"
    shift 2

    options=$(echo "$@" | tr ' ' ',')
    if pcb_is_running "$name"; then
        if [ -z "$options" ]; then
            pcb_cli -q "Process.${BUS_NAME}_$name.loadSharedObject(\"$shared_object\")"
        else
            pcb_cli -q "Process.${BUS_NAME}_$name.loadSharedObject(\"$shared_object\",$options)"
        fi
    fi
}

#usage: mtk_unload <app_name> <so name>
mtk_unload()
{
    name="$1"
    shared_object="$2"
    shift 2

    if pcb_is_running "$name"; then
        pcb_cli -q "Process.${BUS_NAME}_$name.unloadSharedObject(\"$shared_object\")"
    fi
}

#usage: mtk_start <app_name> <so name> <module>
mtk_start()
{
    name="$1"
    shared_object="$2"
    module="$3"
    shift 3

    options=$(echo "$@" | tr ' ' ',')
    if pcb_is_running "$name"; then
        pcb_cli -q "Process.${BUS_NAME}_$name.EnableSync=1"
        pcb_cli -q "Process.${BUS_NAME}_$name.SharedObject.${shared_object}.Module.${module}.start($options)"
    fi
}

#usage: mtk_stop <app_name> <so name> <module>
mtk_stop()
{
    name="$1"
    shared_object="$2"
    module="$3"
    shift 3

    if pcb_is_running "$name"; then
        pcb_cli -q "Process.${BUS_NAME}_$name.EnableSync=1"
        pcb_cli -q "Process.${BUS_NAME}_$name.SharedObject.${shared_object}.Module.${module}.stop()"
    fi
}
